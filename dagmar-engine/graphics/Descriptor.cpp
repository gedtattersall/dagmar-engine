#include "dagmar/Descriptor.h"

/**
 * @brief Default constructor
 */
dag::Descriptor::Descriptor()
{
    fID = SIZE_MAX;
    name = "";
}

/**
 * @brief Constructor with parameters
 */
dag::Descriptor::Descriptor(size_t const& fID, std::string const& name, std::any const& data) :
    fID(fID), name(name), data(data)
{
}
