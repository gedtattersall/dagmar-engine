#include "dagmar/Texture2D.h"

#include <GL/glew.h>
#include <stdexcept>


/**
 * @brief Creates a 2D texture using the information gathered
 * param textureInfo 
 */
void dag::Texture2D::create(dag::TextureInfo const& textureInfo)
{
    this->textureInfo = textureInfo;
	
    // This is done because we want to use modern OpenGL
    // Using the old one would be
    // glGenTextures(1, &id);
    // glBindTexture(GL_TEXTURE_2D, id);
    glCreateTextures(GL_TEXTURE_2D, 1, &id);
    glTextureParameteri(id, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTextureParameteri(id, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTextureParameteri(id, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTextureParameteri(id, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    GLenum format = 0;

    switch (textureInfo.format)
    {
        case TextureFormat::NONE:
        {
            throw std::runtime_error("Error: Invalid Texture Format.");
        }
        case TextureFormat::RGBA8:
        {
            format = GL_RGBA8;
            break;
        }
        case TextureFormat::DEPTH:
        {
            format = GL_DEPTH24_STENCIL8;
            break;
        }
    }

    glTextureStorage2D(id, 1, format, textureInfo.width, textureInfo.height);
}

/**
 * @brief Creates a texture from file
 */
void dag::Texture2D::create(std::string const& path)
{
    // TODO: Implement using stb image
    throw std::runtime_error("Error: function not implemented Texture2D::create(std::string path)");
}