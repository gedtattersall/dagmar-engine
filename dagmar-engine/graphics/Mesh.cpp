#include "dagmar/Mesh.h"

/**
 * @brief Default constructor
 * @param vertices 
 * @param indices 
*/
dag::Mesh::Mesh(std::vector<float> const& vertices, std::vector<uint32_t> const& indices) :
    vertices(vertices), indices(indices)
{
}

/**
 * @brief Generates a VAO with the current data
 * Currently accepting only float, but should be modified to _Vertex
 * @return 
*/
dag::VertexArrayObject<float, uint32_t> dag::Mesh::generateVAO(std::vector<dag::VertexAttribute> const & attributes)
{
    auto vb = dag::VertexBuffer<float>(vertices);
    vb.generate();

    for (auto const& att : attributes)
    {
        vb.addAttribute(att);
    }

    auto ib = dag::IndexBuffer<uint32_t>(indices);
    ib.generate();

    auto vao = dag::VertexArrayObject<float, uint32_t>(vb, ib);
    vao.generate();
    vao.aggregate(GL_STATIC_DRAW);

    return vao;
}