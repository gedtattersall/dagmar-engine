#include "dagmar/Texture.h"
#include <GL/glew.h>

/**
 * @brief Destroy funcs
 */
void dag::Texture::destroy()
{
    if (id != 0)
    {
        glDeleteTextures(1, &id);
    }
}

/**
 * @brief Binds the texture
 */
void dag::Texture::bind(uint32_t const& slot)
{
    glBindTextureUnit(slot, id);
}

/**
 * @brief Unbinds the texture
 */
void dag::Texture::unbind()
{
    glBindTexture(GL_TEXTURE_2D, 0);
}
