#pragma once

#include "dagmar/ShaderProgram.h"

#include <any>
#include <string>
#include <vector>

namespace dag
{
    /**
     * @brief Descriptor class
     *
     * Contains information about the uniform data in a shader
     */
    class Descriptor
    {
      public:
        // Update uniform funcptr id
        size_t fID;
        // Name of the uniform
        std::string name;
        // Data of the uniform
        std::any data;

      public:
        // Default constructor
        Descriptor();

        // Constructor with params
        Descriptor(size_t const& fID, std::string const& name, std::any const& data);
    };
} // namespace dag
