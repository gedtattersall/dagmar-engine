#pragma once
#include "dagmar/Texture.h"
#include <cstdint>
#include <memory>
#include <vector>

namespace dag
{
    /**
     * @brief Framebuffer Texture Info
     */
    struct FramebufferTextureInfo
    {
        TextureFormat format = TextureFormat::NONE;

    	// Default framebuffer texture info
        FramebufferTextureInfo() = default;

    	// Framebuffer Texture Info constructor
        FramebufferTextureInfo(TextureFormat const& format);
    };

	/**
	 * @brief Framebuffer Attachment Info
	 */
    struct FramebufferAttachmentInfo
    {
        std::vector<FramebufferTextureInfo> attachments;

    	// Default framebuffer attachment constructor
        FramebufferAttachmentInfo() = default;

    	// Constructor
        FramebufferAttachmentInfo(std::initializer_list<FramebufferTextureInfo> attachments);
    };

	/**
	 * @brief Framebuffer Info
	 */
    struct FramebufferInfo
    {
        uint32_t width = 0;
        uint32_t height = 0;
        uint32_t samples = 1;
        std::vector<float> clearValues = {1.0f, 1.0f, 1.0f, 1.0f};
        FramebufferAttachmentInfo attachmentInfos;
    };

    /**
     * @brief Framebuffer class
     */
    class Framebuffer
    {
      private:
        uint32_t id = 0;

        // Specification of the framebuffer
        FramebufferInfo info;

        // Buckets
        std::vector<FramebufferTextureInfo> colorAttachmentInfos;
        FramebufferTextureInfo depthAttachmentInfo;

        std::vector<uint32_t> colorAttachmentsIDs;
        uint32_t depthAttachmentID;

      public:
        // Default constructor
        Framebuffer() = default;

    	// Resize framebuffer
        void resize(uint32_t const& width, uint32_t const& height);

    	// Invalidates & recreates the framebuffer
        void invalidate();

    	// Creates the framebuffer
        void create(FramebufferInfo const& framebufferInfo);

    	// Destroys the framebuffer
        void destroy();

    	// Attaches a color texture
        void attachColorTexture(uint32_t const& id, uint32_t const& width, uint32_t const& height, uint32_t const& samples, uint32_t const& format, uint32_t const& index);

    	// Attaches a depth texture
        void attachDepthTexture(uint32_t const& id, uint32_t const& width, uint32_t const& height, uint32_t const& samples, uint32_t const& format, uint32_t type);

    	// Gets the color attachment at id
        uint32_t getColorAttachment(uint32_t const& id);

    	// Gets the depth attachment id
        uint32_t getDepthAttachment();

    	// Binds the framebuffer
        void bind();

    	// Unbind the framebuffer
        void unbind();
    };
} // namespace dag