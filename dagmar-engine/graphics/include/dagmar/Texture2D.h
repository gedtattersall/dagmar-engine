#pragma once
#include <string>

#include "dagmar/Texture.h"

namespace dag
{
    /*
     * @brief Texture2D class
     */
    class Texture2D final : public Texture
    {
    public:
        TextureInfo textureInfo;
    
      public:
    	// Create func with texture info
        virtual void create(TextureInfo const& textureInfo) override;

    	// Create func with path
        virtual void create(std::string const& path);
    };
} // namespace dag