#pragma once
#include <cstdint>

namespace dag
{
    /**
     * @brief Texture format (fixed atm)
     */
    enum class TextureFormat
    {
    	// Invalid
	    NONE = 0,
    	// Colors
    	RGBA8,
    	// Depths /-+/ stencil
    	DEPTH24STENCIL8,
    	// Defaults
    	DEPTH = DEPTH24STENCIL8
    };
	
    /**
     * @brief TextureInfo
     */
	struct TextureInfo
	{
        uint32_t width = 0;
        uint32_t height = 0;
        uint32_t depth = 0;
        TextureFormat format = TextureFormat::NONE;
	};
	
    /**
    * @brief Texture class
    */
    class Texture
    {
      public:
        uint32_t id = 0;

      public:
        Texture() = default;
        virtual ~Texture() = default;

    	// Create func for texture
        virtual void create(TextureInfo const& textureInfo) = 0;

    	// Destroys the texture
        virtual void destroy();

    	// Binds the texture
        virtual void bind(uint32_t const& slot);

    	// Unbinds the texture
        virtual void unbind();
    };
} // namespace dag