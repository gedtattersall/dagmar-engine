#include "dagmar/Framebuffer.h"

#include <GL/glew.h>
#include <stdexcept>

namespace Utility
{
    /**
     * @brief Checks if the texture format is a depth 
     */
    static bool isDepthFormat(dag::TextureFormat const& format)
    {
        switch (format)
        {
            case dag::TextureFormat::DEPTH24STENCIL8:
            {
                return true;
            }
            default:
            {
                return false;
            }
        }
    }
	
    /**
     * @brief Returns the valid enum based on multisampling
     */
    static uint32_t textureTarget(bool const& multisampled)
    {
        return multisampled ? GL_TEXTURE_2D_MULTISAMPLE : GL_TEXTURE_2D;
    }
} // namespace Utility

/*
 * @brief Constructor for the FTI
 */
dag::FramebufferTextureInfo::FramebufferTextureInfo(TextureFormat const& format) :
    format(format)
{
}

/**
 * @brief Constructor for FAI
 */
dag::FramebufferAttachmentInfo::FramebufferAttachmentInfo(std::initializer_list<FramebufferTextureInfo> attachments) :
    attachments(attachments)
{
}

/**
 * @brief Resizes the framebuffer
 */
void dag::Framebuffer::resize(uint32_t const& width, uint32_t const& height)
{
    if (width == 0 || height == 0 || width > 8192 || height > 8192)
    {
        throw std::runtime_error("Error: invalid resize of the framebuffer.");
    }

    info.width = width;
    info.height = height;

    invalidate();
}

/**
 * @brief Invalidates and recreates the buffer
 */
void dag::Framebuffer::invalidate()
{
    if (id)
    {
        glDeleteFramebuffers(1, &id);
        glDeleteTextures(colorAttachmentsIDs.size(), colorAttachmentsIDs.data());
        glDeleteTextures(1, &depthAttachmentID);
    }

    glCreateFramebuffers(1, &id);
    glBindFramebuffer(GL_FRAMEBUFFER, id);

    bool shouldMultisample = info.samples > 1;

    if (!colorAttachmentInfos.empty())
    {
        colorAttachmentsIDs.resize(colorAttachmentInfos.size(), 0);

        glCreateTextures(Utility::textureTarget(shouldMultisample), colorAttachmentsIDs.size(), colorAttachmentsIDs.data());

        std::vector<uint32_t> buffers(colorAttachmentsIDs.size());

        for (size_t i = 0; i < colorAttachmentsIDs.size(); i++)
        {
            glBindTexture(Utility::textureTarget(shouldMultisample), colorAttachmentsIDs[i]);

            switch (colorAttachmentInfos[i].format)
            {
                case dag::TextureFormat::RGBA8:
                {
                    attachColorTexture(colorAttachmentsIDs[i], info.width, info.height, info.samples, GL_RGBA8, i);
                    break;
                }
                default:
                {
                    break;
                }
            }

            buffers[i] = GL_COLOR_ATTACHMENT0 + i;
        }

        glDrawBuffers(buffers.size(), buffers.data());
    }

    if (depthAttachmentInfo.format != TextureFormat::NONE)
    {
        glCreateTextures(Utility::textureTarget(shouldMultisample), 1, &depthAttachmentID);
        glBindTexture(Utility::textureTarget(shouldMultisample), depthAttachmentID);

        switch (depthAttachmentInfo.format)
        {
            case TextureFormat::DEPTH24STENCIL8:
            {
                attachDepthTexture(depthAttachmentID, info.width, info.height, info.samples, GL_DEPTH24_STENCIL8, GL_DEPTH_STENCIL_ATTACHMENT);
                break;
            }
        }
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

/**
 * @brief Creates the framebuffer
 */
void dag::Framebuffer::create(FramebufferInfo const& framebufferInfo)
{
    this->info = framebufferInfo;

    for (auto& ai : framebufferInfo.attachmentInfos.attachments)
    {
        if (!Utility::isDepthFormat(ai.format))
        {
            colorAttachmentInfos.emplace_back(ai);
        }
        else
        {
            depthAttachmentInfo = ai;
        }
    }

    invalidate();
}

/**
 * @brief Destroys the framebuffer
 */
void dag::Framebuffer::destroy()
{
    if (id != 0)
    {
        glDeleteFramebuffers(1, &id);
    }
}

/**
 * @brief Attaches the color texture
 */
void dag::Framebuffer::attachColorTexture(uint32_t const& id, uint32_t const& width, uint32_t const& height, uint32_t const& samples, uint32_t const& format, uint32_t const& index)
{
    bool multisampled = samples > 1;

    if (multisampled)
    {
        glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, samples, format, width, height, GL_FALSE);
    }
    else
    {
        glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    }

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + index, Utility::textureTarget(multisampled), id, 0);
}

/**
 * @brief Attaches the depth texture
 */
void dag::Framebuffer::attachDepthTexture(uint32_t const& id, uint32_t const& width, uint32_t const& height, uint32_t const& samples, uint32_t const& format, uint32_t type)
{
    bool multisampled = samples > 1;

    if (multisampled)
    {
        glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, samples, format, width, height, GL_FALSE);
    }
    else
    {
        glTexStorage2D(GL_TEXTURE_2D, 1, format, width, height);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    }

    glFramebufferTexture2D(GL_FRAMEBUFFER, type, Utility::textureTarget(multisampled), id, 0);
}

/**
 * @brief Get color attachment at index
 */
uint32_t dag::Framebuffer::getColorAttachment(uint32_t const& id)
{
    if (id < colorAttachmentsIDs.size())
    {
        return colorAttachmentsIDs[id];
    }
}

/**
 * @brief Get depth attachment
 */
uint32_t dag::Framebuffer::getDepthAttachment()
{
    return depthAttachmentID;
}

/**
 * @brief Binds the framebuffer
 */
void dag::Framebuffer::bind()
{
    glBindFramebuffer(GL_FRAMEBUFFER, id);
    glViewport(0, 0, info.width, info.height);

    glClearColor(info.clearValues[0], info.clearValues[1], info.clearValues[2], info.clearValues[3]);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

/**
 * @brief Unbinds the framebuffer
 */
void dag::Framebuffer::unbind()
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
