#pragma once

#define GLM_FORCE_RADIANS
#include <cstddef>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "dagmar/Descriptor.h"

namespace dag
{
    struct Transform
    {
        glm::vec3 position;
        glm::vec3 rotation;
        glm::vec3 scale = glm::vec3(1.0f);
        // bool isDirty; - possible future improvement for when to call update
        glm::mat4 modelMatrix = glm::mat4(1.0f);
        void updateModelMatrix(){
            modelMatrix = glm::scale(glm::mat4(1.0f), scale);
            modelMatrix = glm::rotate(modelMatrix, rotation.x, glm::vec3(1.0f, 0.0f, 0.0f));
            modelMatrix = glm::rotate(modelMatrix, rotation.y, glm::vec3(0.0f, 1.0f, 0.0f));
            modelMatrix = glm::rotate(modelMatrix, rotation.z, glm::vec3(0.0f, 0.0f, 1.0f));
            modelMatrix = glm::translate(glm::mat4(1.0f), position);
        }
    };

    struct RigidBody
    {
        glm::vec3 velocity;
        glm::vec3 acceleration;
        glm::vec3 netForce;
        glm::vec3 externalForce;
        glm::vec3 impulse;
        float mass;
    };

    // Only used in the Rendering System
    struct DescriptorSet
    {
        size_t vaoID;
        size_t shaderProgramID;
        std::vector<Descriptor> descriptors;
    };

    // centre and radius of bounding sphere
    struct Collision
    {
        float radius;
        glm::vec3 center;
    };

    struct Name
    {
        std::string name;
    };
} // namespace dag
