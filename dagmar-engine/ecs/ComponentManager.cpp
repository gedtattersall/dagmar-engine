#include "dagmar/ComponentManager.h"

/**
 * @brief ComponentManager constructor
 */
dag::ComponentManager::ComponentManager()
{
    nextComponentType = {};
}

/**
 * @brief Marks the component as being destroyed in all arrays
 */
void dag::ComponentManager::entityDestroyed(dag::Entity const& entity)
{
    // Notify each component array about the destroyed entity
    // it will remove it if existent
    for (auto const& pair : componentArrays)
    {
        auto const& component = pair.second;
        component->entityDestroyed(entity);
    }
}
