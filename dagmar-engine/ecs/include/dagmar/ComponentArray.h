#pragma once

#include <array>
#include <unordered_map>

#include "Entity.h"
#include "dagmar/Exceptions.h"

namespace dag
{
    /**
     * @brief Interface for the component array
     */
    class InterfaceComponentArray
    {
      public:
        virtual ~InterfaceComponentArray() = default;
        virtual void entityDestroyed(Entity const& entity) = 0;
    };

    /**
     * @brief A component array (usually for each type T of a component)
     */
    template <typename T>
    class ComponentArray : public InterfaceComponentArray
    {
      private:
        std::array<T, MAX_ENTITIES> components;
        std::unordered_map<Entity, size_t> entityToIndex;
        std::unordered_map<size_t, Entity> indexToEntity;
        size_t validEntries = 0;

      public:
        /**
         * @brief Inserts the component data
         */
        void insertData(Entity const& entity, T const& component)
        {
            if (entityToIndex.contains(entity))
            {
                throw dag::ECSException(dag::ECSException::ErrorType::InsertComponentAlreadyExistent);
            }

            size_t nextIndex = validEntries;
            entityToIndex[entity] = nextIndex;
            indexToEntity[nextIndex] = entity;
            components[nextIndex] = component;
            ++validEntries;
        }

        /**
         * @brief Tries to remove the component data
         */
        void removeData(Entity const& entity)
        {
            if (!entityToIndex.contains(entity))
            {
                throw dag::ECSException(dag::ECSException::ErrorType::RemoveComponentNotExistent);
            }

            size_t removedEntityIndex = entityToIndex[entity];
            size_t lastElementIndex = validEntries - 1;
            // Swapping
            components[removedEntityIndex] = components[lastElementIndex];

            // Updating the maps
            Entity entityOfLastElement = indexToEntity[lastElementIndex];
            entityToIndex[entityOfLastElement] = removedEntityIndex;
            indexToEntity[removedEntityIndex] = entityOfLastElement;

            entityToIndex.erase(entity);
            indexToEntity.erase(lastElementIndex);

            validEntries--;
        }

        /**
         * @brief Tries to get the component data
         */
        T& getData(Entity const& entity)
        {
            if (!entityToIndex.contains(entity))
            {
                throw dag::ECSException(dag::ECSException::ErrorType::GetComponentNotExistent);
            }

            return components[entityToIndex[entity]];
        }

        /*
         * @brief Tries to destroy the entity
         */
        void entityDestroyed(Entity const& entity) override
        {
            if (entityToIndex.contains(entity))
            {
                // Remove if existent
                removeData(entity);
            }
        }
    };
} // namespace dag
