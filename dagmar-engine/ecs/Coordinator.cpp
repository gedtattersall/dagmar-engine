#include "dagmar/Coordinator.h"
#include "dagmar/PhysicsSystem.h"
#include <mutex>

#include <iostream>

bool dag::Coordinator::isShuttingDown()
{
    return shuttingDown.load();
}

/**
 * @brief Startup function overriden for the module class
 */
void dag::Coordinator::startup()
{
    entityManager = std::make_unique<EntityManager>();
    componentManager = std::make_unique<ComponentManager>();
    systemManager = std::make_unique<SystemManager>();
}

/**
 * @brief Shutdown function overriden for the module class
 */
void dag::Coordinator::shutdown()
{
    if (systemManager)
    {
        if (dag::coordinator->containsSystem<PhysicsSystem>())
        {
            std::lock_guard<std::mutex> lock(dag::coordinator->getSystem<PhysicsSystem>()->physicsMutex);
            shuttingDown = true;
        }
    }

    entityManager.reset();
    componentManager.reset();
    systemManager.reset();   
}

/**
 * @brief Gets the name of the module
 */
std::string dag::Coordinator::getName() const
{
    return "Coordinator";
}

/**
 * @brief Creates an entity
 */
dag::Entity dag::Coordinator::createEntity()
{
    return entityManager->createEntity();
}

/**
 * @brief Destroys an entity then signals the managers
 * that the entity has been destroyed
 */
void dag::Coordinator::destroyEntity(dag::Entity const& entity)
{
    entityManager->destroyEntity(entity);
    componentManager->entityDestroyed(entity);
    systemManager->entityDestroyed(entity);
}
