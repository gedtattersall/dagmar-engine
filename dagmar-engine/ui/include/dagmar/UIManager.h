#pragma once
#include "dagmar/Module.h"
#include "dagmar/WindowManager.h"
#include <memory>
#include <filesystem>

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

#include "dagmar/RenderingSystem.h"

namespace dag
{
    /**
     * @brief UIManager defines ImGUI elements
     *
     */
    class UIManager : public Module
    {
      private:
        std::filesystem::path currentFileManagerPath;
        std::filesystem::path currentFileManagerSelectedPath;

        bool renderWindowFocused = false;

    	std::shared_ptr<dag::RenderingSystem> renderingSystem;

      public:
        ImVec2 viewportSize = ImVec2(0, 0);

      public:
        UIManager() = default;
        ~UIManager() = default;

        void startup() override;

        void shutdown() override;

        std::string getName() const override;

    	// Shows the top bar
		void drawTopBar();
    	
        // draws the whole UI
        void drawMainUI(unsigned int texColorBuffer);

        // draws the opengl window
        void drawOpenGLRenderWindow(unsigned int texColorBuffer);

        // draws a physics control window
        void drawPhysicsSystem();

        // draws file manager
        void drawFileManager();

        void drawCameraController();
        // draws the scene graph
        void drawSceneGraph();

        // draws the inspector
        void drawInspector();
    };

    inline std::shared_ptr<UIManager> uiManager(new UIManager());
} // namespace dag