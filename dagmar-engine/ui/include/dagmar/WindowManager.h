#pragma once
#include "dagmar/Log.h"
#include "dagmar/Module.h"
#include <memory>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

namespace dag
{
    /**
     * @brief WindowManager takes care of any operations to do with initialising destroying the UI
     *
     */
    class WindowManager : public Module
    {
      public:
        GLFWwindow* window;
        ImGuiContext* ctx;

        int width, height;

        WindowManager() = default;
        ~WindowManager() = default;

        void startup() override;

        void shutdown() override;

        std::string getName() const override;

        static void windowSizeCallback(GLFWwindow* window, int width, int height);
    };

    inline std::shared_ptr<WindowManager> windowManager(new WindowManager());
} // namespace dag