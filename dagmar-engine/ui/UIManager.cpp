#include "dagmar/UIManager.h"
#include "dagmar/Coordinator.h"
#include "dagmar/FileSystem.h"
#include "dagmar/PhysicsSystem.h"
#include "dagmar/Scene.h"
#include "dagmar/WindowManager.h"

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

#include <array>
#include <filesystem>

#include "dagmar/Renderer.h"
#include "dagmar/ResourceManager.h"

/**
 * @brief Starts up the module
 */
void dag::UIManager::startup()
{
    currentFileManagerPath = dag::filesystem::getAssetPath();
    currentFileManagerSelectedPath = "";

	renderingSystem = coordinator->getSystem<RenderingSystem>();
    glfwMaximizeWindow(windowManager->window);
}

/**
 * @brief Shuts down the module
 */
void dag::UIManager::shutdown()
{
}

/**
 * @brief Gets the name of the module
 */
std::string dag::UIManager::getName() const
{
    return "UIManager";
}

void dag::UIManager::drawTopBar()
{
    // Rendering the menu bar
    if (ImGui::BeginMainMenuBar())
    {
        if (ImGui::BeginMenu("File"))
        {
        	if (ImGui::MenuItem("New Scene", "CTRL+N"))
            {
            }
        	
            if (ImGui::MenuItem("Save", "CTRL+S"))
            {
            }
        	
            if (ImGui::MenuItem("Save As", "CTRL+SHIFT+S"))
            {
            }
        	
            ImGui::EndMenu();
        }

        if (ImGui::BeginMenu("Edit"))
        {
            if (ImGui::MenuItem("Undo", "CTRL+Z", false, false))
            {
            }
        	
            if (ImGui::MenuItem("Redo", "CTRL+Y", false, false))
            {
            }
        	
            ImGui::Separator();
        	
            if (ImGui::MenuItem("Cut", "CTRL+X"))
            {
            }
        	
            if (ImGui::MenuItem("Copy", "CTRL+C"))
            {
            }
        	
            if (ImGui::MenuItem("Paste", "CTRL+V"))
            {
            }

        	ImGui::Separator();
        	
            if (ImGui::MenuItem("Duplicate", "CTRL+D"))
            {
            }
        	
            if (ImGui::MenuItem("Rename", "F2"))
            {
            }
        	
            if (ImGui::MenuItem("Delete", "DEL"))
            {
            }
        	
            ImGui::Separator();
        	
            if (ImGui::MenuItem("Play", "CTRL+P"))
            {
            }
        	
            if (ImGui::MenuItem("Pause", "F2", false, false))
            {
            }
        	
            if (ImGui::MenuItem("Reset"))
            {
            }

            ImGui::EndMenu();
        }

        if (ImGui::BeginMenu("Window"))
        {
            if (ImGui::BeginMenu("Open Window"))
            {
                float sz = ImGui::GetTextLineHeight();

            	ImGui::Text("TODO: Current Example");
            	
                for (int i = 0; i < ImGuiCol_COUNT; i++)
                {
                    const char* name = ImGui::GetStyleColorName((ImGuiCol)i);
                    ImVec2 p = ImGui::GetCursorScreenPos();
                    ImGui::GetWindowDrawList()->AddRectFilled(p, ImVec2(p.x + sz, p.y + sz), ImGui::GetColorU32((ImGuiCol)i));
                    ImGui::Dummy(ImVec2(sz, sz));
                    ImGui::SameLine();
                    ImGui::MenuItem(name);
                }

            	ImGui::EndMenu();
            }
        	
        	if (ImGui::MenuItem("Reset Windows Layouts"))
        	{
        	}
        	
            ImGui::EndMenu();
        }
    	
        if (ImGui::BeginMenu("Help"))
        {
            ImGui::Text("This is Dagmar Engine.");

            ImGui::EndMenu();
        }

        ImGui::EndMainMenuBar();
    }
}

/**
 * @brief Draws the file manager
 *
 */
void dag::UIManager::drawFileManager()
{
    ImGui::Begin("File Manager");

    static int item_current_idx = 0;

    if (ImGui::BeginListBox("filelistbox", ImVec2(-FLT_MIN, -1)))
    {
        int n = 0;

        if (!std::filesystem::equivalent(currentFileManagerPath, dag::filesystem::getAssetPath()))
        {
            const bool is_selected = (item_current_idx == n);

            if (ImGui::Selectable(std::filesystem::path("..").string().c_str(), is_selected))
                item_current_idx = n;

            if (is_selected)
                ImGui::SetItemDefaultFocus();
            ++n;

            if (ImGui::IsItemHovered() && ImGui::IsMouseDoubleClicked(0))
            {
                currentFileManagerPath = currentFileManagerPath.parent_path();
                Log::debug(currentFileManagerPath.string());
            }
            ++n;
        }

        for (auto& p : fs::directory_iterator(currentFileManagerPath))
        {
            const bool is_selected = (item_current_idx == n);

            std::filesystem::path path = std::filesystem::relative(p, currentFileManagerPath).make_preferred();
            std::string path_string = path.generic_string();
            if (p.is_directory())
            {
                path_string += "/";
            }

            if (ImGui::Selectable(path_string.c_str(), is_selected))
                item_current_idx = n;

            if (is_selected)
                ImGui::SetItemDefaultFocus();

            if (p.is_directory())
            {
                if (ImGui::IsItemHovered() && ImGui::IsMouseDoubleClicked(0))
                {
                    currentFileManagerPath /= path;
                    Log::debug(currentFileManagerPath.string());
                }
            }
            else
            {
                if (ImGui::IsItemHovered() && ImGui::IsMouseDoubleClicked(0))
                {
                    dag::scene->createEntity("Spawned Entity");

                    auto last_ent = dag::scene->entities.back();

                    coordinator->addComponent(last_ent, dag::Transform{glm::vec3(0.0f, 0.0f, 0.0f)});
                    coordinator->addComponent(last_ent, dag::RigidBody{.impulse = glm::vec3(20.0f, 0.0f, 0.0f), .mass = 0.05f});
                    coordinator->addComponent(last_ent, dag::Collision{.radius = 0.1, .center = glm::vec3(0.0f)});

                    coordinator->getComponent<dag::Transform>(last_ent).updateModelMatrix();

                    dag::DescriptorSet descriptorSet;
                    // Default shader
                    descriptorSet.shaderProgramID = 0;

                    resourceManager->parseOBJ(p.path());

                    dag::Mesh mesh;
                    resourceManager->getMesh(0, mesh);

                    std::vector<dag::VertexAttribute> atts = {
                        dag::VertexAttribute(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0),
                        dag::VertexAttribute(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)))};

                    auto rs = dag::coordinator->getSystem<dag::RenderingSystem>();

                    rs->vaos.push_back(mesh.generateVAO(atts));

                    descriptorSet.vaoID = rs->vaos.size() - 1;

                    dag::Descriptor descriptorModel;
                    descriptorModel.fID = dag::RenderingSystem::UniformFuncs::UniformMatrix4fv;
                    descriptorModel.name = "model";
                    descriptorModel.data = coordinator->getComponent<dag::Transform>(last_ent).modelMatrix;

                    descriptorSet.descriptors.push_back(descriptorModel);
                    coordinator->addComponent(last_ent, descriptorSet);
                }
            }

            ++n;
        }

        ImGui::EndListBox();
    }

    ImGui::End();
}

/**
 * @brief Draws the window containing our Scene
 *
 */
void dag::UIManager::drawOpenGLRenderWindow(unsigned int texColorBuffer)
{
    bool test = false;

    ImGui::Begin("Game Window", &test, ImGuiWindowFlags_MenuBar);

    if (ImGui::BeginMenuBar())
    {
        ImVec4 col = ImVec4(128.0 / 255.0, 23.0 / 255.0, 15.0 / 255.0, 1);
        ImGui::PushID(0);
        ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(2 / 7.0f, 0.6f, 0.6f));
        ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(2 / 7.0f, 0.7f, 0.7f));
        ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(2 / 7.0f, 0.8f, 0.8f));
        if (ImGui::ArrowButton("##right", ImGuiDir_Right))
        {
            std::cout << "Pressed" << std::endl;
        }
        ImGui::PopStyleColor(3);
        ImGui::PopID();

        ImGui::PushID(0);
        ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(1 / 7.0f, 0.6f, 0.6f));
        ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(1 / 7.0f, 0.7f, 0.7f));
        ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(1 / 7.0f, 0.8f, 0.8f));
        if (ImGui::Button("||"))
        {
            std::cout << "Pressed" << std::endl;
        }
        ImGui::PopStyleColor(3);
        ImGui::PopID();

        ImGui::PushID(0);
        ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(0 / 7.0f, 0.6f, 0.6f));
        ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(0 / 7.0f, 0.7f, 0.7f));
        ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(0 / 7.0f, 0.8f, 0.8f));
        if (ImGui::Button("Reset"))
        {
            std::cout << "Pressed" << std::endl;
        }
        ImGui::PopStyleColor(3);
        ImGui::PopID();

        ImGui::EndMenuBar();
    }

    ImGui::BeginChild("GameRender");

    auto wsize = ImGui::GetWindowSize();

    if (ImGui::IsWindowFocused())
    {
        renderWindowFocused = true;
    }
    else
    {
        renderWindowFocused = false;
    }

    ImGui::Image((ImTextureID)(uintptr_t)texColorBuffer, wsize, ImVec2(0, 1), ImVec2(1, 0));

    ImGui::EndChild();

    ImGui::End();

    if (wsize.x != viewportSize.x || wsize.y != viewportSize.y)
    {
        viewportSize = wsize;
        renderingSystem->cameras[renderingSystem->currentCameraID].setAspect(viewportSize.x / viewportSize.y);
        renderer->compositeFramebuffer.resize(wsize.x, wsize.y);
    }
}
/**
 * @brief Draws the inspector for selected entity
 *
 */
void dag::UIManager::drawInspector()
{

    auto const& entity = dag::scene->entities[dag::scene->currentEntity];

    ImGui::Begin("Inspector");
    auto& nameComponent = coordinator->getComponent<Name>(entity);

    char field[35];
    std::strcpy(field, nameComponent.name.c_str());

    if (ImGui::InputText("Name", field, 35))
    {
        nameComponent.name = std::string(field);
        dag::scene->recomputeSceneGraph();
    }

    try
    {
        auto& transform = coordinator->getComponent<Transform>(entity);
        if (ImGui::CollapsingHeader("Transform"))
        {

            ImGui::DragFloat3("Position", glm::value_ptr(transform.position), 0.01f);
            ImGui::DragFloat3("Rotation", glm::value_ptr(transform.rotation), 0.01f);
            ImGui::DragFloat3("Scale", glm::value_ptr(transform.scale), 0.01f);
        }
    }
    catch (const std::exception& e)
    {
        // do nothing
    }

    try
    {
        auto& rigidBody = coordinator->getComponent<RigidBody>(entity);
        if (ImGui::CollapsingHeader("Rigid body"))
        {
            ImGui::DragFloat("Mass", &rigidBody.mass, 0.01f);
        }
    }
    catch (const std::exception& e)
    {
        // do nothing
    }

    try
    {
        auto& collision = coordinator->getComponent<Collision>(entity);
        if (ImGui::CollapsingHeader("Collision"))
        {

            ImGui::DragFloat("Radius of collision sphere", &collision.radius, 0.01f);
        }
    }
    catch (const std::exception& e)
    {
        // do nothing
    }

    ImGui::End();
}

/**
 * @brief Draws the scene graph
 *
 */
void dag::UIManager::drawSceneGraph()
{
    ImGui::Begin("Scene Graph");
    ImGui::ListBox("", &(dag::scene->currentEntity), dag::scene->sceneGraph.data(), dag::scene->sceneGraph.size());
    ImGui::End();
}

/**
 * @brief Draws the controls for the physics system 
 *
 */
void dag::UIManager::drawPhysicsSystem()
{
    ImGui::Begin("Physics System");

    ImGuiColorEditFlags flags = ImGuiColorEditFlags_DisplayRGB;

    if (ImGui::Button("Reset Scene"))
    {
        int i = 0;
        for (auto const& entity : coordinator->getSystem<PhysicsSystem>()->entities)
        {
            auto& transform = coordinator->getComponent<dag::Transform>(entity);
            auto& rigidBody = coordinator->getComponent<dag::RigidBody>(entity);
            if (i == 0)
            {
                transform = std::move(dag::Transform{glm::vec3(-1.0f, 0.0f, 0.0f)});
                rigidBody = std::move(dag::RigidBody{.impulse = glm::vec3(20.0f, 0.0f, 0.0f), .mass = 0.05f});
            }
            else
            {
                transform = std::move(dag::Transform{glm::vec3(1.0f, 0.0f, 0.0f)});
                rigidBody = std::move(dag::RigidBody{.impulse = glm::vec3(-20.0f, 0.0f, 0.0f), .mass = 0.049f});
            }
            ++i;
        }
    }
    ImGui::SliderFloat("Time Multiplier", &(coordinator->getSystem<PhysicsSystem>()->timeMultiplier), 0.1f, 2.0f);

    ImGui::SliderFloat3("Gravity", &(coordinator->getSystem<PhysicsSystem>()->gravity[0]), -10.0f, 10.0f);

    ImGui::SliderFloat("Air Resistance", &(coordinator->getSystem<PhysicsSystem>()->airResistance), 0.0f, 1.0f);
    ImGui::SliderFloat("Epsilon", &(coordinator->getSystem<PhysicsSystem>()->epsilon), 0.0f, 1.0f);
    ImGui::SliderFloat("Dispersion Coefficient", &(coordinator->getSystem<PhysicsSystem>()->dispersionCoefficient), 0.0f, 1.0f);
    ImGui::SliderFloat("Plastic Coefficient", &(coordinator->getSystem<PhysicsSystem>()->plasticCoefficient), 0.0f, 1.0f);

    ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
    ImGui::End();
}

/**
 * @brief Draws the entire UI
 *
 */
void dag::UIManager::drawMainUI(unsigned int texColorBuffer)
{
    //glBindFramebuffer(GL_FRAMEBUFFER, 0);
    {
        int display_w, display_h;
        glfwGetFramebufferSize(windowManager->window, &display_w, &display_h);

        glViewport(0, 0, display_w, display_h);

        // new frame from helper functions
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        ImGui::DockSpaceOverViewport(ImGui::GetMainViewport());

        drawTopBar();
        drawOpenGLRenderWindow(texColorBuffer);
        drawPhysicsSystem();
        drawFileManager();
        drawSceneGraph();
        drawInspector();

        ImGui::ShowDemoWindow();

        ImGui::Render();

        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        ImGuiIO& io = ImGui::GetIO();
        (void)io;

        if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
        {
            GLFWwindow* backup_current_context = glfwGetCurrentContext();
            ImGui::UpdatePlatformWindows();
            ImGui::RenderPlatformWindowsDefault();
            glfwMakeContextCurrent(backup_current_context);
        }
    }
}