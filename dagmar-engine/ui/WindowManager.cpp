#include "dagmar/WindowManager.h"
#include "dagmar/Log.h"
#include "dagmar/Module.h"
#include "dagmar/Renderer.h"
#include <memory>
#include <filesystem>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

void dag::WindowManager::startup()
{
    width = 1024;
    height = 768;

    // Initialise GLFW
    if (!glfwInit())
    {
        Log::error("Failed to initialize GLFW");
        throw std::runtime_error("Failed to initialize GLFW");
    }

    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    // turn v-sync off, faster fps, flushing instead of buffer swapping
    glfwWindowHint(GLFW_DOUBLEBUFFER, GL_FALSE);

    // Open a window and create its OpenGL context
    window = glfwCreateWindow(1024, 768, "Dagmar Engine", NULL, NULL);

    if (window == NULL)
    {
        glfwTerminate();
        Log::error("Failed to open GLFW window.");
        throw std::runtime_error("Failed to open GLFW window.");
    }
    glfwMakeContextCurrent(window);

    // Initialize GLEW
    if (glewInit() != GLEW_OK)
    {
        glfwTerminate();
        Log::error("Failed to initialize GLEW");
        throw std::runtime_error("Failed to initialize GLEW");
    }
	
    // Initialise imgui helper functions
    IMGUI_CHECKVERSION();
    ImGuiContext* ctx = ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO();

    try
    {
        if (!std::filesystem::exists(io.IniFilename))
        {
            std::filesystem::copy("default_imgui.ini", "imgui.ini");
        }
    }
    catch (std::exception& e)
    {
        Log::error(e.what());
        throw e;
    }

    (void)io;
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard; // Enable Keyboard Controls
    io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;     // Enable Docking
#ifdef _WIN32
    io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;   // Enable Multi-Viewport / Platform Windows
#endif

    ImGuiStyle& style = ImGui::GetStyle();
    if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
    {
        style.WindowRounding = 0.0f;
        style.Colors[ImGuiCol_WindowBg].w = 1.0f;
    }

    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init("#version 450");

    // Ensure we can capture the escape key being pressed below
    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
#ifndef ENGINE_BUILD
    glfwSetWindowSizeCallback(window, windowSizeCallback);
#endif
}

void dag::WindowManager::shutdown()
{
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    // Close OpenGL window and terminate GLFW
    glfwDestroyWindow(window);
    glfwTerminate();
}

std::string dag::WindowManager::getName() const
{
    return "WindowManager";
}

void dag::WindowManager::windowSizeCallback(GLFWwindow* window, int width, int height)
{
    windowManager->width = width;
    windowManager->height = height;
    renderer->windowSizeCallback(window, width, height);
}
