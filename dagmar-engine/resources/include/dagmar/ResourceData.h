#pragma once
#include <cstdlib>
#include <glm/glm.hpp>
#include <string>
#include <vector>


namespace dag
{
    /**
     * @brief Vertex Data structure
     * 
     * Contains Vertex information: vertices(position), normals, textcoords, colors
     */
    struct VertexData
    {
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec3> normals;
        std::vector<glm::vec2> textcoords;
        std::vector<glm::vec3> colors;
    };

    /**
     * @brief Face Data structure
     * 
     * Contains indices to vertices, normals, texcoords
     */
    struct FaceData
    {
        std::vector<int> vertexIndex;
        std::vector<int> normalIndex;
        std::vector<int> texcoordIndex;
    };

    /**
     * @brief Shape Data structure
     * 
     * Contains the shape name and the faces of that shape
     */
    struct ShapeData
    {
        std::string shapeName;
        std::vector<FaceData> faces;
    };

    /**
     * @brief Material info, class for re usability
     */
    class MaterialData
    {
      public:
        std::string name = "";
        glm::vec3 ambience = glm::vec3(0.0f);
        glm::vec3 diffuse = glm::vec3(0.0f);
        glm::vec3 specular = glm::vec3(0.0f);
        glm::vec3 emission = glm::vec3(0.0f);
        float dissolve = 0.0f;

        std::string textureAmbience = "";
        std::string textureDiffuse = "";
        std::string textureSpecular = "";
        std::string textureAlpha = "";

      public:
        // Defaulted Constructor
        MaterialData() = default;
    };

    /**
     * @brief Model class
     * 
     * Contains information about the vertex data, shapes and materials
     * Model -> Shapes -> Faces -> VertexIndices
     */
    class Model
    {
      public:
        VertexData vertices;
        std::vector<ShapeData> shapes;
        std::vector<MaterialData> materials;
        glm::vec3 center;
        float radius;
    };
} // namespace dag