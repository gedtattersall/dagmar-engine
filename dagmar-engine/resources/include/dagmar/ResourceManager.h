#pragma once
#include "dagmar/ResourceData.h"
#include "glm/glm.hpp"

#include <filesystem>
#include <string>
#include <vector>

#include "dagmar/Log.h"
#include "dagmar/Module.h"
#include "dagmar/Mesh.h"

namespace dag
{
    /**
     * @brief Resource Manager
     */
    class ResourceManager : public dag::Module
    {
      public:
        //models -> parseOBJ(), writeBin()
        std::vector<Model> models;

        //outModels ->readBin()
        std::vector<Model> outModels;

      public:
        ResourceManager() = default;

        // Override startup for the module
        void startup() override;

        // Override shutdown for the module
        void shutdown() override;

        // Override name for the module
        std::string getName() const override;

        //Obj file to main memory
        void parseOBJ(std::filesystem::path const& inputfile);

        //Main memory to Binary file(.dat)
        void writeBin(std::filesystem::path const& outPath);

        //Binary File to Main Memory
        void readBin(std::filesystem::path const& datPath);

        //Copy material information to a MaterialData class object
        void copyMaterial(std::vector<MaterialData> mat, size_t const& modelIndex);

        //File formatting
        std::string getObjName(std::string const& filename);

        void getMesh(size_t const& index, dag::Mesh& outputMesh);
    };

    inline std::shared_ptr<ResourceManager> resourceManager(new ResourceManager());
} // namespace dag