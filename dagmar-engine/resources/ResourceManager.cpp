#include "dagmar/ResourceManager.h"
#include "dagmar/FileSystem.h"
#include "dagmar/Module.h"
#include <cstring>
#include <fstream>
#include <iostream>
#include <tiny_obj_loader.h>

/**
 * @brief Startup function for the resource manager
 */
void dag::ResourceManager::startup()
{
}

/*
 * @brief Shutdown function for the resource manager
 */
void dag::ResourceManager::shutdown()
{
}

/*
 * @brief Get name of the module
 */
std::string dag::ResourceManager::getName() const
{

    return "ResourceManager";
}

/**
 * @brief Parse object file to main memory
 */
void dag::ResourceManager::parseOBJ(std::filesystem::path const& inputfile)
{
    Model tempModel;

    //Tinyobj declarations
    std::string basepath = inputfile.parent_path().string();
    tinyobj::attrib_t attrib;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;
    bool triangulate = true;
    std::string warn;
    std::string err;

    std::string filePath = inputfile.string();

    Log::debug(inputfile);

    //Load
    bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, filePath.c_str(), basepath.c_str(), triangulate);

    //Error Check
    if (!warn.empty())
    {
        Log::warning(warn);
    }

    if (!err.empty())
    {
        Log::error(err);
    }

    if (!ret)
    {
        Log::error("Failed to load/parse .obj.");
        exit(EXIT_FAILURE);
    }

    //Vertices
    glm::vec3 tempAttribute;
    glm::vec2 tempTexCoord;

    tempModel.center = glm::vec3(0.0f);

    for (size_t v = 0; v < attrib.vertices.size() / 3; v++)
    {
        tempAttribute.x = (attrib.vertices[3 * v + 0]);
        tempAttribute.y = (attrib.vertices[3 * v + 1]);
        tempAttribute.z = (attrib.vertices[3 * v + 2]);
        tempModel.vertices.vertices.push_back(tempAttribute);

        tempModel.center += tempAttribute;
    }
    // centre of the model and radius of bounding sphere for the collision
    tempModel.center /= static_cast<float>(attrib.vertices.size() / 3);
    float maxRadius = 0.0f;
    for (size_t v = 0; v < attrib.vertices.size() / 3; v++)
    {
        float length = glm::length(tempModel.vertices.vertices[v] - tempModel.center);
        if (length > maxRadius)
        {
            maxRadius = length;
        }
    }
    tempModel.radius = maxRadius;

    for (size_t v = 0; v < attrib.normals.size() / 3; v++)
    {
        tempAttribute.x = (attrib.normals[3 * v + 0]);
        tempAttribute.y = (attrib.normals[3 * v + 1]);
        tempAttribute.z = (attrib.normals[3 * v + 2]);
        tempModel.vertices.normals.push_back(tempAttribute);
    }

    for (size_t v = 0; v < attrib.texcoords.size() / 2; v++)
    {
        tempTexCoord.x = (attrib.texcoords[2 * v + 0]);
        tempTexCoord.y = (attrib.texcoords[2 * v + 1]);
        tempModel.vertices.textcoords.push_back(tempTexCoord);
    }

    for (size_t v = 0; v < attrib.colors.size() / 3; v++)
    {
        tempAttribute.x = (attrib.colors[3 * v + 0]);
        tempAttribute.y = (attrib.colors[3 * v + 1]);
        tempAttribute.z = (attrib.colors[3 * v + 2]);
        tempModel.vertices.colors.push_back(tempAttribute);
    }

    //Shapes -> Faces -> Vertex Index
    ShapeData tempShapes;
    FaceData tempFace;

    // For each Shape
    for (size_t i = 0; i < shapes.size(); i++)
    {
        tempShapes.shapeName = shapes[i].name;

        //For vertex indexing
        size_t vertex_offset = 0;

        //For each Face
        for (size_t f = 0; f < shapes[i].mesh.num_face_vertices.size(); f++)
        {
            //No. of vert->Triangulation = 3;
            size_t fnum = shapes[i].mesh.num_face_vertices[f];

            // For each vertex in the face
            for (size_t v = 0; v < fnum; v++)
            {
                tinyobj::index_t idx = shapes[i].mesh.indices[vertex_offset + v];

                tempFace.vertexIndex.push_back(idx.vertex_index);
                // If -1 stored, no value provided
                tempFace.normalIndex.push_back(idx.normal_index);
                tempFace.texcoordIndex.push_back(idx.texcoord_index);
            }
            vertex_offset += fnum;
        }
        tempShapes.faces.push_back(tempFace);
        tempModel.shapes.push_back(tempShapes);
    }

    //Materials
    dag::MaterialData tempMaterial;

    for (size_t i = 0; i < materials.size(); i++)
    {
        tempMaterial.name = materials[i].name;

        tempMaterial.ambience.x = materials[i].ambient[0];
        tempMaterial.ambience.y = materials[i].ambient[1];
        tempMaterial.ambience.z = materials[i].ambient[2];

        tempMaterial.diffuse.x = materials[i].diffuse[0];
        tempMaterial.diffuse.y = materials[i].diffuse[1];
        tempMaterial.diffuse.z = materials[i].diffuse[2];

        tempMaterial.specular.x = materials[i].specular[0];
        tempMaterial.specular.y = materials[i].specular[1];
        tempMaterial.specular.z = materials[i].specular[2];

        tempMaterial.emission.x = materials[i].emission[0];
        tempMaterial.emission.y = materials[i].emission[1];
        tempMaterial.emission.z = materials[i].emission[2];

        tempMaterial.dissolve = materials[i].dissolve;

        //Respective textures
        tempMaterial.textureAmbience = materials[i].ambient_texname;
        tempMaterial.textureDiffuse = materials[i].diffuse_texname;
        tempMaterial.textureSpecular = materials[i].specular_texname;
        tempMaterial.textureAlpha = materials[i].alpha_texname;

        tempModel.materials.push_back(tempMaterial);
    }
    models.push_back(tempModel);
}

/**
 * @brief Formats the file for obj naming
 */
std::string dag::ResourceManager::getObjName(std::string const& filename)
{
    return filename.substr(0, filename.find(".obj"));
}

/**
 * @brief Writes the resource to binary (.dat)
 */
void dag::ResourceManager::writeBin(std::filesystem::path const& outPath)
{
    std::string objFilePath = outPath.string();
    std::string outBinName = getObjName(std::string(objFilePath));

    if (std::string(objFilePath).empty())
    {
        //Call getFilename / setFilename first.
        Log::error("Error: filename not set.");
        exit(EXIT_FAILURE);
    }

    outBinName = outBinName.append(".dat");

    std::ofstream outBinFile;
    outBinFile.open(outBinName, std::ios::out | std::ios::binary);

    //For keeping track of the number of entities while reading
    size_t tempSize = 0;

    //Number of Models
    tempSize = models.size();
    outBinFile.write((char*)&tempSize, sizeof(size_t));
    for (size_t i = 0; i < models.size(); i++)
    {
        //Vertices
        //Position
        tempSize = models[i].vertices.vertices.size();
        outBinFile.write((char*)&tempSize, sizeof(size_t));
        outBinFile.write((char*)&models[i].vertices.vertices[0], sizeof(glm::vec3) * tempSize);
        //Normals
        tempSize = models[i].vertices.normals.size();
        outBinFile.write((char*)&tempSize, sizeof(size_t));
        outBinFile.write((char*)&models[i].vertices.normals[0], sizeof(glm::vec3) * tempSize);
        //Texture Coordinates
        tempSize = models[i].vertices.textcoords.size();
        outBinFile.write((char*)&tempSize, sizeof(size_t));
        outBinFile.write((char*)&models[i].vertices.textcoords[0], sizeof(glm::vec2) * tempSize);

        tempSize = models[i].vertices.colors.size();
        outBinFile.write((char*)&tempSize, sizeof(size_t));
        outBinFile.write((char*)&models[i].vertices.colors[0], sizeof(glm::vec3) * tempSize);

        //Shapes
        tempSize = models[i].shapes.size();
        outBinFile.write((char*)&tempSize, sizeof(size_t));
        for (size_t j = 0; j < models[i].shapes.size(); j++)
        {
            tempSize = strlen(models[i].shapes[j].shapeName.c_str());
            outBinFile.write((char*)&tempSize, sizeof(size_t));
            outBinFile.write(models[i].shapes[j].shapeName.c_str(), tempSize);

            tempSize = models[i].shapes[j].faces.size();
            outBinFile.write((char*)&tempSize, sizeof(size_t));
            for (size_t k = 0; k < models[i].shapes[j].faces.size(); k++)
            {
                // VertexIndicies
                tempSize = models[i].shapes[j].faces[k].vertexIndex.size();
                outBinFile.write((char*)&tempSize, sizeof(size_t));
                outBinFile.write((char*)&models[i].shapes[j].faces[k].vertexIndex[0], sizeof(int) * tempSize);
                //Normal Indices
                tempSize = models[i].shapes[j].faces[k].normalIndex.size();
                outBinFile.write((char*)&tempSize, sizeof(size_t));
                outBinFile.write((char*)&models[i].shapes[j].faces[k].normalIndex[0], sizeof(int) * tempSize);
                //TexCoord Indidces
                tempSize = models[i].shapes[j].faces[k].texcoordIndex.size();
                outBinFile.write((char*)&tempSize, sizeof(size_t));
                outBinFile.write((char*)&models[i].shapes[j].faces[k].texcoordIndex[0], sizeof(int) * tempSize);
            }
        }

        //Materials
        tempSize = models[i].materials.size();
        outBinFile.write((char*)&tempSize, sizeof(size_t));
        for (size_t j = 0; j < models[i].materials.size(); j++)
        {
            tempSize = strlen(models[i].materials[j].name.c_str());
            outBinFile.write((char*)&tempSize, sizeof(size_t));
            outBinFile.write(models[i].materials[j].name.c_str(), tempSize);
            outBinFile.write((char*)&models[i].materials[j].ambience, sizeof(glm::vec3));
            outBinFile.write((char*)&models[i].materials[j].diffuse, sizeof(glm::vec3));
            outBinFile.write((char*)&models[i].materials[j].specular, sizeof(glm::vec3));
            outBinFile.write((char*)&models[i].materials[j].emission, sizeof(glm::vec3));
            outBinFile.write((char*)&models[i].materials[j].dissolve, sizeof(float));

            tempSize = strlen(models[i].materials[j].textureAmbience.c_str());
            outBinFile.write((char*)&tempSize, sizeof(size_t));
            outBinFile.write(models[i].materials[j].textureAmbience.c_str(), tempSize);

            tempSize = strlen(models[i].materials[j].textureDiffuse.c_str());
            outBinFile.write((char*)&tempSize, sizeof(size_t));
            outBinFile.write(models[i].materials[j].textureDiffuse.c_str(), tempSize);

            tempSize = strlen(models[i].materials[j].textureSpecular.c_str());
            outBinFile.write((char*)&tempSize, sizeof(size_t));
            outBinFile.write(models[i].materials[j].textureSpecular.c_str(), tempSize);

            tempSize = strlen(models[i].materials[j].textureAlpha.c_str());
            outBinFile.write((char*)&tempSize, sizeof(size_t));
            outBinFile.write(models[i].materials[j].textureAlpha.c_str(), tempSize);
        }
    }

    outBinFile.close();
}

/**
 * @brief Read Binary File to Main Memory
 */
void dag::ResourceManager::readBin(std::filesystem::path const& datPath)
{
    std::string datFileDirec = datPath.string();

    if (std::string(datFileDirec).empty())
    {
        //Call getFilename / setFilename first.
        Log::error("Error: filename not set.");
        exit(EXIT_FAILURE);
    }

    std::ifstream inBinFile(datFileDirec, std::ios::in | std::ios::binary);

    size_t nModels;
    size_t nShapes;
    size_t nFaces;
    size_t nMaterials;

    size_t tempSize = 0;

    inBinFile.read((char*)&nModels, sizeof(size_t));
    outModels.resize(nModels);

    for (size_t i = 0; i < nModels; i++)
    {
        //Vertices
        //Position
        inBinFile.read((char*)&tempSize, sizeof(size_t));
        outModels[i].vertices.vertices.resize(tempSize);
        inBinFile.read((char*)outModels[i].vertices.vertices.data(), sizeof(glm::vec3) * tempSize);
        //Normals
        inBinFile.read((char*)&tempSize, sizeof(size_t));
        outModels[i].vertices.normals.resize(tempSize);
        inBinFile.read((char*)outModels[i].vertices.normals.data(), sizeof(glm::vec3) * tempSize);
        //Texture Coordinates
        inBinFile.read((char*)&tempSize, sizeof(size_t));
        outModels[i].vertices.textcoords.resize(tempSize);
        inBinFile.read((char*)outModels[i].vertices.textcoords.data(), sizeof(glm::vec2) * tempSize);
        //Colours
        inBinFile.read((char*)&tempSize, sizeof(size_t));
        outModels[i].vertices.colors.resize(tempSize);
        inBinFile.read((char*)outModels[i].vertices.colors.data(), sizeof(glm::vec3) * tempSize);

        //Shapes
        inBinFile.read((char*)&nShapes, sizeof(size_t));
        outModels[i].shapes.resize(nShapes);
        for (size_t j = 0; j < nShapes; j++)
        {
            inBinFile.read((char*)&tempSize, sizeof(size_t));
            outModels[i].shapes[j].shapeName.resize(tempSize);
            inBinFile.read(&outModels[i].shapes[j].shapeName[0], tempSize);

            inBinFile.read((char*)&nFaces, sizeof(size_t));
            outModels[i].shapes[j].faces.resize(nFaces);
            for (size_t k = 0; k < nFaces; k++)
            {

                // VertexIndicies
                inBinFile.read((char*)&tempSize, sizeof(size_t));
                outModels[i].shapes[j].faces[k].vertexIndex.resize(tempSize);
                inBinFile.read((char*)outModels[i].shapes[j].faces[k].vertexIndex.data(), sizeof(int) * tempSize);
                //Normal Indices
                inBinFile.read((char*)&tempSize, sizeof(size_t));
                outModels[i].shapes[j].faces[k].normalIndex.resize(tempSize);
                inBinFile.read((char*)outModels[i].shapes[j].faces[k].normalIndex.data(), sizeof(int) * tempSize);
                //TexCoord Indidces
                inBinFile.read((char*)&tempSize, sizeof(size_t));
                outModels[i].shapes[j].faces[k].texcoordIndex.resize(tempSize);
                inBinFile.read((char*)outModels[i].shapes[j].faces[k].texcoordIndex.data(), sizeof(int) * tempSize);
            }
        }

        inBinFile.read((char*)&nMaterials, sizeof(size_t));
        outModels[i].materials.resize(nMaterials);
        for (size_t j = 0; j < nMaterials; j++)
        {
            inBinFile.read((char*)&tempSize, sizeof(size_t));
            outModels[i].materials[j].name.resize(tempSize);
            inBinFile.read(&outModels[i].materials[j].name[0], models[i].materials[j].name.size());
            inBinFile.read((char*)&outModels[i].materials[j].ambience, sizeof(glm::vec3));
            inBinFile.read((char*)&outModels[i].materials[j].diffuse, sizeof(glm::vec3));
            inBinFile.read((char*)&outModels[i].materials[j].specular, sizeof(glm::vec3));
            inBinFile.read((char*)&outModels[i].materials[j].emission, sizeof(glm::vec3));
            inBinFile.read((char*)&outModels[i].materials[j].dissolve, sizeof(float));

            inBinFile.read((char*)&tempSize, sizeof(size_t));
            outModels[i].materials[j].textureAmbience.resize(tempSize);
            inBinFile.read(&outModels[i].materials[j].textureAmbience[0], models[i].materials[j].textureAmbience.size());

            inBinFile.read((char*)&tempSize, sizeof(size_t));
            outModels[i].materials[j].textureDiffuse.resize(tempSize);
            inBinFile.read(&outModels[i].materials[j].textureDiffuse[0], models[i].materials[j].textureDiffuse.size());

            inBinFile.read((char*)&tempSize, sizeof(size_t));
            outModels[i].materials[j].textureSpecular.resize(tempSize);
            inBinFile.read(&outModels[i].materials[j].textureSpecular[0], models[i].materials[j].textureSpecular.size());

            inBinFile.read((char*)&tempSize, sizeof(size_t));
            outModels[i].materials[j].textureAlpha.resize(tempSize);
            inBinFile.read(&outModels[i].materials[j].textureAlpha[0], models[i].materials[j].textureAlpha.size());
        }
    }

    inBinFile.close();
}

/**
 * @brief Copy material information to a MaterialData class object.
 */
void dag::ResourceManager::copyMaterial(std::vector<dag::MaterialData> mat, size_t const& modelIndex)
{
    for (size_t i = 0; i < models[modelIndex].materials.size(); i++)
    {
        mat.push_back(models[modelIndex].materials[i]);
    }
}

void dag::ResourceManager::getMesh(size_t const& index, dag::Mesh& outputMesh)
{
    std::vector<float> vertices(models[index].vertices.vertices.size() * 6);

    for (int v = 0; v < models[index].vertices.vertices.size(); ++v)
    {
        vertices[6 * v] = models[index].vertices.vertices[v].x;
        vertices[6 * v + 1] = models[index].vertices.vertices[v].y;
        vertices[6 * v + 2] = models[index].vertices.vertices[v].z;

    	if (v < models[index].vertices.normals.size())
    	{
    		vertices[6 * v + 3] = models[index].vertices.normals[v].x;
	        vertices[6 * v + 4] = models[index].vertices.normals[v].y;
	        vertices[6 * v + 5] = models[index].vertices.normals[v].z;
    	}
        else
        {
            vertices[6 * v + 3] = 0;
            vertices[6 * v + 4] = 1;
            vertices[6 * v + 5] = 0;
        }
    }


    std::vector<uint32_t> indices;
    for (int s = 0; s < models[index].shapes.size(); ++s)
    {
        for (int f = 0; f < models[index].shapes[s].faces.size(); ++f)
        {
            for (int i = 0; i < models[index].shapes[s].faces[f].vertexIndex.size(); ++i)
            {
                indices.emplace_back(models[index].shapes[s].faces[f].vertexIndex[i]);
            }
        }
    }

    for (auto& i : indices)
    {
        Log::debug(i);
    }

    outputMesh = dag::Mesh(vertices, indices);
}
