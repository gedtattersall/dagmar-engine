#include <GL/glew.h>
#include <dagmar/Log.h>
#include <filesystem>
#include <fstream>
#include <climits>
#include <sstream>
#include <string>


#include "dagmar/Exceptions.h"
#include "dagmar/ShaderModule.h"
#include "dagmar/ShaderProgram.h"


/**
 * @brief Simple shader program
 */
dag::ShaderProgram::ShaderProgram()
{
    id = UINT_MAX;
}

/**
 * @brief Constructor for the shader program
 *
 * Given a list of shaders, it constructs a valid shader pipeline
 */
dag::ShaderProgram::ShaderProgram(std::vector<std::shared_ptr<dag::ShaderModule>>& shaders) :
    id(UINT_MAX)
{
    for (auto shader0 : shaders)
    {
        for (auto shader1 : shaders)
        {
            if ((shader0 != shader1) && (shader0->type == shader1->type))
            {
                Log::warning("Trying to attach multiple shaders of the same type to a shader program!");
            }
        }
    }

    id = glCreateProgram();

    for (auto const& shader : shaders)
    {
        glAttachShader(id, shader->id);
    }

    int num_shaders;

    glGetProgramiv(id, GL_ATTACHED_SHADERS, &num_shaders);

    int success;
    char infoLog[1024];

    glLinkProgram(id);
    // print linking errors if any
    glGetProgramiv(id, GL_LINK_STATUS, &success);
    if (!success)
    {
        glGetProgramInfoLog(id, 1024, NULL, infoLog);
        Log::error("SHADER::PROGRAM::LINKING_FAILED");
        Log::error(infoLog);
        throw dag::ShaderProgramLinkingException();
    }
}

/**
 * @brief Shader Program destructor
 */
void dag::ShaderProgram::destroy()
{
    glDeleteProgram(id);
}

/**
 * @brief Uses the shader
 */
void dag::ShaderProgram::bind()
{
    glUseProgram(id);
}

/**
 * @brief Unbinds the shader
 */
void dag::ShaderProgram::unbind()
{
    glUseProgram(0);
}
