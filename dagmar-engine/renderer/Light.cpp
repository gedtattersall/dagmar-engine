#include "dagmar/Light.h"

/**
 * @brief Default constructor
 */
dag::Light::Light()
{
    this->position = glm::vec3(0.0f, 0.0f, 0.0f);
}

/**
 * @brief Setter for light position
 */
void dag::Light::setPosition(glm::vec3 const& pos)
{
    this->position = pos;
}

/**
 * @brief Getter for light position
 */
glm::vec3 dag::Light::getPosition() const
{
    return this->position;
}