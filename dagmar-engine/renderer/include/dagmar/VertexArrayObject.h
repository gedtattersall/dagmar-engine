#pragma once

#include <memory>

#include "dagmar/IndexBuffer.h"
#include "dagmar/VertexBuffer.h"

namespace dag
{
    /**
     * @brief VertexArrayObject
     *
     * Responsible for composing a VAO.
     * @tparam T
     * @tparam V
    */
    template <class T, class V>
    class VertexArrayObject
    {
      public:
        uint32_t id = 0;
        VertexBuffer<T> vertexBuffer;
        IndexBuffer<V> indexBuffer;

      public:
        VertexArrayObject() = default;

        /**
         * @brief Default constructor
         * @param vertexBuffer 
         * @param indexBuffer 
        */
        VertexArrayObject(VertexBuffer<T> const& vertexBuffer, IndexBuffer<V> const& indexBuffer) :
            vertexBuffer(vertexBuffer), indexBuffer(indexBuffer)
        {
        }

        /**
         * @brief Destruction function for VAO
        */
        void destroy()
        {
            if (id != 0)
            {
                glDeleteVertexArrays(1, &id);
            }
        }

        /**
         * @brief Generate function for VAO
        */
        void generate()
        {
            if (id == 0)
            {
                glGenVertexArrays(1, &id);
            }
        }

        /**
         * @brief Creates the VAO from the vertex buffer & index buffer
         * @param mode 
        */
        void aggregate(int32_t const& mode = GL_TRIANGLES)
        {
            if (id != 0)
            {
                bind();

                vertexBuffer.bind();
                vertexBuffer.load(mode);
                vertexBuffer.activateAttributes();

                indexBuffer.bind();
                indexBuffer.load(mode);

                unbind();

                indexBuffer.unbind();
                vertexBuffer.unbind();
            }
        }

        /**
         * @brief Bind the VAO
        */
        void bind()
        {
            glBindVertexArray(id);
        }

        /**
         * @brief Unbind the VAO
        */
        void unbind()
        {
            glBindVertexArray(0);
        }

        /**
         * @brief Draw method for VAO (dependent on what buffers we have)
         */
        void draw(int32_t const& mode = GL_TRIANGLES)
        {
            bind();

            if (indexBuffer.elements.size() && vertexBuffer.elements.size())
            {
                indexBuffer.draw(mode);
            }
            else if (vertexBuffer.elements.size())
            {
                vertexBuffer.draw(mode);
            }

            unbind();
        }
    };
} // namespace dag
