#pragma once

#include "Buffer.h"

#include <cstdint>
#include <GL/glew.h>

namespace dag
{
    /**
     * @brief Vertex Buffer Class
     */
    template <class T>
    class IndexBuffer : public Buffer<T>
    {
        public:
            IndexBuffer() = default;
            
            // Default old-style constructor
            IndexBuffer(void* data, uint32_t const& size) : Buffer<T>(data, size) { }

            // Constructor with a vector
            IndexBuffer(std::vector<T> const& data) : Buffer<T>(data) { }

            // Copy constructor
            IndexBuffer(IndexBuffer const& other) : Buffer<T>(other) { }

            // Pure virtual update function
            virtual void update(void* data, uint32_t const& offset, bool const& flushToGPU) override
            {
                if (flushToGPU)
                {
                    bind();
                    glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, sizeof(T) * offset, sizeof(T), data);
                    unbind();
                }

                this->elements[offset] = *(reinterpret_cast<T*>(data));
            }

            // Virtual function to map the buffer
            virtual void* map(int32_t const& access) override
            {
                this->mapping = glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, access);
                return this->mapping;
            }

            // Virtual function to map the buffer
            virtual void* mapRange(uint32_t const& offset, uint32_t const& length, int32_t const& access) override
            {
                this->mapping = glMapBufferRange(GL_ELEMENT_ARRAY_BUFFER, offset, length, access);
                return this->mapping;
            }

            // Virtual function to unmap the buffer
            virtual void unmap() override
            {
                glUnmapBuffer(GL_ARRAY_BUFFER);
            }

            // Virtual function to load the buffer
            virtual void load(int32_t const& usage) override
            {
                glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->elements.size() * sizeof(T), this->elements.data(), usage);
            }

            // Virtual fucntion to bind the buffer
            virtual void bind() override
            {
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->id);
            }

            // Virtual function to unbind the buffer
            virtual void unbind() override
            {
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
            }

            // Virtual function to draw via the indexbuffer
            // Requires a vertex buffer to be bound before
            void draw(int32_t const& mode)
            {
                glDrawElements(mode, static_cast<uint32_t>(this->elements.size()), GL_UNSIGNED_INT, 0);
            }
    };
}
