#pragma once

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace dag
{
    /**
     * @brief Light class
     */
    class Light
    {
      private:
        glm::vec3 position;
        
      public:
        // default constructor with default values
        Light();

        void setPosition(glm::vec3 const& pos);
        glm::vec3 getPosition() const;
    };
} // namespace dag
