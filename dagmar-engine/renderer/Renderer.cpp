#include "dagmar/Renderer.h"
#include "dagmar/Components.h"
#include "dagmar/Coordinator.h"
#include "dagmar/FileSystem.h"
#include "dagmar/Module.h"
#include "dagmar/RenderingSystem.h"
#include "dagmar/ShaderModule.h"
#include "dagmar/ShaderProgram.h"
#include "dagmar/UIManager.h"
#include "dagmar/VertexArrayObject.h"
#include "dagmar/VertexAttribute.h"
#include "dagmar/VertexBuffer.h"
// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

#include <memory>

/**
 * @brief Instanciate our framebuffer to draw to, it's attachments, and our texture.
 */
void dag::Renderer::startup()
{
    {
        std::vector<float> vertices =
            {
                1.0f, 1.0f, 1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 0.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f, 1.0f};

        std::vector<uint32_t> indices =
            {
                0, 1, 3, 1, 2, 3};

        screenMesh = dag::Mesh(vertices, indices);

        std::vector<dag::VertexAttribute> atts = {dag::VertexAttribute(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0),
                                                  dag::VertexAttribute(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)(2 * sizeof(float)))};

        screenMeshVAO = screenMesh.generateVAO(atts);

        // Creating shaders
        std::vector<std::shared_ptr<dag::ShaderModule>> shaders =
            {
                std::make_shared<dag::ShaderModule>(dag::filesystem::getAssetPath() / "shaders/screen_shader.vert", GL_VERTEX_SHADER),
                std::make_shared<dag::ShaderModule>(dag::filesystem::getAssetPath() / "shaders/screen_shader.frag", GL_FRAGMENT_SHADER)};

        // Create shader programs
        screenShader = dag::ShaderProgram(shaders);
    }

    // Engine assumption: shaders[0] == default

    // Creating shaders
    std::vector<std::shared_ptr<dag::ShaderModule>> shaders =
        {
            std::make_shared<dag::ShaderModule>(dag::filesystem::getAssetPath() / "shaders/default/default.vert", GL_VERTEX_SHADER),
            std::make_shared<dag::ShaderModule>(dag::filesystem::getAssetPath() / "shaders/default/default.frag", GL_FRAGMENT_SHADER)};

    // Create shader programs
    auto sp = dag::ShaderProgram(shaders);

    coordinator->registerComponent<dag::Transform>();
    coordinator->registerComponent<dag::DescriptorSet>();

    dag::Signature renderingSystemSignature;
    renderingSystemSignature.set(coordinator->getComponentType<dag::Transform>());
    renderingSystemSignature.set(coordinator->getComponentType<dag::DescriptorSet>());

    renderingSystem = coordinator->registerSystem<dag::RenderingSystem>();
    renderingSystem->coordinator = coordinator;
    coordinator->setSystemSignature<dag::RenderingSystem>(renderingSystemSignature);

    renderingSystem->shaderPrograms.push_back(sp);

    // Fill the system with data
    renderingSystem->initializeCameraDescriptors();

    FramebufferInfo compositeFramebufferInfo;
    compositeFramebufferInfo.attachmentInfos = {{TextureFormat::RGBA8}, {TextureFormat::DEPTH}};
    compositeFramebufferInfo.width = 1024;
    compositeFramebufferInfo.height = 768;
    compositeFramebufferInfo.clearValues = {0.25f, 0.25f, 0.25f, 1.0f};
    compositeFramebuffer.create(compositeFramebufferInfo);
}

/**
 * @brief Clean up shaders, framebuffer, render buffer, etc.
 */
void dag::Renderer::shutdown()
{
    compositeFramebuffer.destroy();

    // Destroys the used shader modules
    for (auto& shader : renderingSystem->shaderPrograms)
    {
        shader.destroy();
    }
}

/**
 * @brief Get the name of the class for Module
 */
std::string dag::Renderer::getName() const
{
    return "Renderer";
}

/**
 * @brief Main draw call. This is the entry point for drawing the whole application.
 */
void dag::Renderer::draw()
{
    compositeFramebuffer.bind();
    renderingSystem->update();
    compositeFramebuffer.unbind();

    // Backbuffer
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glDisable(GL_DEPTH_TEST);
    glViewport(0, 0, windowManager->width, windowManager->height);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

#ifdef ENGINE_BUILD
    uiManager->drawMainUI(compositeFramebuffer.getColorAttachment(0));
    glfwSwapBuffers(dag::windowManager->window);
#else
    screenShader.bind();
    screenMeshVAO.bind();
    glBindTexture(GL_TEXTURE_2D, compositeFramebuffer.getColorAttachment(0));
    screenMeshVAO.draw();
    screenMeshVAO.unbind();
    screenShader.unbind();
    glFlush();
#endif
}

void dag::Renderer::windowSizeCallback(GLFWwindow* window, int width, int height)
{
#ifndef ENGINE_BUILD
    auto renderSys = coordinator->getSystem<RenderingSystem>();
    renderSys->cameras[renderSys->currentCameraID].setAspect(width / static_cast<float>(height));
    compositeFramebuffer.resize(width, height);
#endif
}