#include "dagmar/VertexAttribute.h"

#include <GL/glew.h>

/**
 * @brief Creates a placeholder for a vertex attribute so that it can be turned asynchronously
 *
 * It is also known in advance.
 *
 * @param index
 * @param size
 * @param type
 * @param normalized
 * @param stride
 * @param offsetPtr
*/
dag::VertexAttribute::VertexAttribute(uint32_t const& index,
                                      int32_t const& size,
                                      uint32_t const& type,
                                      bool const& normalized,
                                      int32_t stride,
                                      void* offsetPtr) :
    index(index),
    size(size), type(type), normalized(normalized),
    stride(stride), offsetPtr(offsetPtr){
                        // Empty constructor
                    };

/**
 * @brief Activates the vertex attribute pointer
 *
 * Must be called within a vertex buffer bind
*/
void dag::VertexAttribute::activate()
{
    glVertexAttribPointer(index, size, type, normalized, stride, offsetPtr);
    glEnableVertexAttribArray(index);
}
