#pragma once
#include <filesystem>
#include <iostream>
#include <vector>
#include <fstream>
#include <stdlib.h>
#include "glm/glm.hpp"
#include "glm/vec2.hpp"
#include "glm/vec3.hpp"

#include "dagmar/Module.h"

namespace dag
{
    
    inline std::shared_ptr<ResourceManager> resourceManager(new ResourceManager());
}