#include "dagmar/FileSystem.h"

#ifdef _WIN32
#include <windows.h>
#else
#include <limits.h>
#include <unistd.h>
#endif

/**
 * @brief Gets the executable path
 * Platform independent function that gets the absolute path of the executable
 */
fs::path dag::filesystem::getExePath()
{
#ifdef _WIN32
    wchar_t path[MAX_PATH] = { 0 };
    GetModuleFileNameW(NULL, path, MAX_PATH);
    return path;
#else
    char result[PATH_MAX];
    ssize_t count = readlink("/proc/self/exe", result, PATH_MAX);
    return std::string(result, (count > 0) ? count : 0);
#endif
}

/**
 * @brief Gets the assets directory path
 * The assets directory contains engine-specific assets (e.g. objs)
 */
fs::path dag::filesystem::getAssetPath()
{
    fs::path path = std::filesystem::path(ASSETS_FOLDER).make_preferred();
    return path;
}

/**
 * @brief Gets the intermediate directory path
 * Intermediate directory should contain binary info from the projects
 */
fs::path dag::filesystem::getIntermediatePath()
{
    fs::path path = (getExePath().parent_path().parent_path() / "intermediate").make_preferred();
    return path;
}
