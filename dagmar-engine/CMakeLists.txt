set(DAGMAR_ENGINE_BINARY_DIR ${CMAKE_CURRENT_BINARY_DIR})
set(DAGMAR_ENGINE_INSTALL_DIR ${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_BINDIR})

add_subdirectory(log)
add_subdirectory(filesystem)
add_subdirectory(platform)
add_subdirectory(renderer)
add_subdirectory(exceptions)
add_subdirectory(allocator)
add_subdirectory(module_lifecycle)
add_subdirectory(resources)
add_subdirectory(ecs)
add_subdirectory(ui)
add_subdirectory(graphics)
add_subdirectory(components)
add_subdirectory(systems)
add_subdirectory(scene)
add_subdirectory(timer)

file(WRITE ${CMAKE_CURRENT_BINARY_DIR}/dummy.cpp "")
add_library(dagmar_engine_lib SHARED
	${CMAKE_CURRENT_BINARY_DIR}/dummy.cpp
)

add_library(Dagmar ALIAS dagmar_engine_lib)

target_link_libraries(dagmar_engine_lib
	glfw::glfw
	glm::glm
	imgui::imgui
	stb::stb
	glew::glew
	tinyobjloader::tinyobjloader
    Dagmar::Log
    Dagmar::Filesystem
    Dagmar::PlatformDetector
	Dagmar::Renderer
	Dagmar::Exceptions
	Dagmar::Allocator
	Dagmar::ModuleLifecycle
	Dagmar::Resources
	Dagmar::UI
    Dagmar::Graphics
	Dagmar::Components
	Dagmar::Systems
	Dagmar::Scene
)

add_executable(dagmar_engine
	main.cpp
)

target_link_libraries(dagmar_engine
	Dagmar
)

install(TARGETS dagmar_engine_lib)
install(TARGETS dagmar_engine)
