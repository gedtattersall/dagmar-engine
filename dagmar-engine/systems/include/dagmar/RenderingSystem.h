#pragma once
#include "dagmar/Camera.h"
#include "dagmar/Light.h"
#include "dagmar/Coordinator.h"
#include "dagmar/Descriptor.h"
#include "dagmar/ShaderProgram.h"
#include "dagmar/System.h"
#include "dagmar/VertexArrayObject.h"
#include <cstdint>
#include <functional>
#include <vector>

namespace dag
{
    class RenderingSystem : public System
    {
      public:
        // Mainly used as a map
        enum UniformFuncs : size_t
        {
            Uniform1f,
            Uniform1i,
            Uniform1ui,
            Uniform2fv,
            Uniform3fv,
            Uniform4fv,
            Uniform2iv,
            Uniform3iv,
            Uniform4iv,
            Uniform2uiv,
            Uniform3uiv,
            Uniform4uiv,
            UniformMatrix4fv
        };

      public:
        std::shared_ptr<Coordinator> coordinator;

        std::vector<VertexArrayObject<float, uint32_t>> vaos;
        std::vector<ShaderProgram> shaderPrograms;
        // Shader data for constant global stuff: lights, camera, etc
        std::vector<std::function<void(Descriptor const&)>> shaderUpdateFuncs;

        std::vector<Descriptor> cameraDescriptors;
        std::vector<Descriptor> lightDescriptors;

        // TODO Better entity style for lights & camera
        std::vector<Camera> cameras;
        std::vector<Light> lights;

        inline static uint32_t currentShaderBound;
        size_t currentCameraID = UINT32_MAX;

      public:
        RenderingSystem();
        ~RenderingSystem() override = default;
        void initializeCameraDescriptors();
        // void initializeLightDescriptors();

        // TODO Add destroy mechanism
        void addCamera(Camera const& camera);
        void removeCamera(size_t const& cameraID);
        void setActiveCamera(size_t const& cameraID);

        void addLight(Light const& light);
        void removeLight(size_t const& lightID);

        void update();

      private:
        static void wrapUniform1f(Descriptor const& descriptor);
        static void wrapUniform1i(Descriptor const& descriptor);
        static void wrapUniform1ui(Descriptor const& descriptor);
        static void wrapUniform2fv(Descriptor const& descriptor);
        static void wrapUniform3fv(Descriptor const& descriptor);
        static void wrapUniform4fv(Descriptor const& descriptor);
        static void wrapUniform2iv(Descriptor const& descriptor);
        static void wrapUniform3iv(Descriptor const& descriptor);
        static void wrapUniform4iv(Descriptor const& descriptor);
        static void wrapUniform2uiv(Descriptor const& descriptor);
        static void wrapUniform3uiv(Descriptor const& descriptor);
        static void wrapUniform4uiv(Descriptor const& descriptor);
        static void wrapUniformMatrix4fv(Descriptor const& descriptor);
    };
} // namespace dag
