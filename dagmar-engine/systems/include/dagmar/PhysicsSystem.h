#pragma once

#include "dagmar/Components.h"
#include "dagmar/Coordinator.h"

#include <mutex>
#include <memory>
#include <thread>

/**
 * @brief Physics system that deals with collision and applying forces
  */

namespace dag
{
    class PhysicsSystem : public System
    {
      public:
        std::shared_ptr<Coordinator> coordinator;
        glm::vec3 gravity;
        float airResistance;
        float epsilon;
        float dispersionCoefficient;
        float plasticCoefficient;
        float timeMultiplier;

        std::mutex physicsMutex;
        std::thread physicsThread;

      public:
        PhysicsSystem();
        ~PhysicsSystem() override;

        void startup();

        void update(float const& dt);
        void checkForCollisions();
    };
} // namespace dag