#include "dagmar/RenderingSystem.h"
#include "dagmar/Components.h"
#include "dagmar/Descriptor.h"
#include "dagmar/KeyManager.h"

#include "glm/gtc/type_ptr.hpp"

/**
 * @brief Default rendering system constructor
 */
dag::RenderingSystem::RenderingSystem()
{
    shaderUpdateFuncs.emplace_back(RenderingSystem::wrapUniform1f);
    shaderUpdateFuncs.emplace_back(RenderingSystem::wrapUniform1i);
    shaderUpdateFuncs.emplace_back(RenderingSystem::wrapUniform1ui);
    shaderUpdateFuncs.emplace_back(RenderingSystem::wrapUniform2fv);
    shaderUpdateFuncs.emplace_back(RenderingSystem::wrapUniform3fv);
    shaderUpdateFuncs.emplace_back(RenderingSystem::wrapUniform4fv);
    shaderUpdateFuncs.emplace_back(RenderingSystem::wrapUniform2iv);
    shaderUpdateFuncs.emplace_back(RenderingSystem::wrapUniform3iv);
    shaderUpdateFuncs.emplace_back(RenderingSystem::wrapUniform4iv);
    shaderUpdateFuncs.emplace_back(RenderingSystem::wrapUniform2uiv);
    shaderUpdateFuncs.emplace_back(RenderingSystem::wrapUniform3uiv);
    shaderUpdateFuncs.emplace_back(RenderingSystem::wrapUniform4uiv);
    shaderUpdateFuncs.emplace_back(RenderingSystem::wrapUniformMatrix4fv);

    addCamera(Camera());
    setActiveCamera(0);
    cameras[currentCameraID].setPosition(glm::vec3(0.0f, 0.0f, 5.0f));
    cameras[currentCameraID].setNear(1.0f);
    cameras[currentCameraID].setFar(10000.0f);

    currentShaderBound = UINT32_MAX;

	addLight(Light());
    lights.back().setPosition(glm::vec3(0, 0, 100));
}

/**
 * @brief Initializes the descriptors for the camera
*/
void dag::RenderingSystem::initializeCameraDescriptors()
{
    cameraDescriptors.emplace_back(Descriptor(UniformFuncs::UniformMatrix4fv, "view", cameras[currentCameraID].getView()));
    cameraDescriptors.emplace_back(Descriptor(UniformFuncs::UniformMatrix4fv, "projection", cameras[currentCameraID].getProjection()));
    cameraDescriptors.emplace_back(Descriptor(UniformFuncs::Uniform3fv, "cameraPosition", cameras[currentCameraID].getPosition()));
}

/**
 * @brief Adds a camera
 * @param camera 
*/
void dag::RenderingSystem::addCamera(Camera const& camera)
{
    cameras.push_back(camera);
}

/**
 * @brief Removes a camera (at least 1 camera is present)
 * @param cameraID 
*/
void dag::RenderingSystem::removeCamera(size_t const& cameraID)
{
    if (cameraID < 0 || cameraID >= cameras.size())
    {
        throw std::runtime_error("Camera index out of bounds.");
    }

    if (currentCameraID == cameraID)
    {
        // We need at least one camera
        if (cameras.size() > 1)
        {
            currentCameraID = 0;
            cameras.erase(cameras.begin() + cameraID, cameras.begin() + cameraID + 1);
        }
    }
}

/**
 * @brief Sets the active camera
 * @param cameraID 
*/
void dag::RenderingSystem::setActiveCamera(size_t const& cameraID)
{
    if (cameraID < 0 || cameraID >= cameras.size())
    {
        throw std::runtime_error("Camera index out of bounds.");
    }

    currentCameraID = cameraID;
}

/**
 * @brief Adds a light
 * @param light
*/
void dag::RenderingSystem::addLight(Light const& light)
{
    lights.push_back(light);
    lightDescriptors.emplace_back(Descriptor(UniformFuncs::Uniform3fv, "lightPosition", lights.back().getPosition()));
}

/**
 * @brief Removes a light
 * @param lightID 
*/
void dag::RenderingSystem::removeLight(size_t const& lightID)
{
    if (lightID < 0 || lightID >= cameras.size())
    {
        throw std::runtime_error("Camera index out of bounds.");
    }

    lights.erase(lights.begin() + lightID, lights.begin() + lightID + 1);
    lightDescriptors.erase(lightDescriptors.begin() + lightID, lightDescriptors.end() + lightID + 1);
}


/**
 * @brief RenderingSystem update (rendering main func)
 */
void dag::RenderingSystem::update()
{
    if (dag::keyManager->isKeyDown(GLFW_KEY_W))
    {
        cameras[currentCameraID].moveForward(5.0);
    }
    if (dag::keyManager->isKeyDown(GLFW_KEY_S))
    {
        cameras[currentCameraID].moveForward(-5.0);
    }
    if (dag::keyManager->isKeyDown(GLFW_KEY_A))
    {
        cameras[currentCameraID].moveRight(-5.0);
    }
    if (dag::keyManager->isKeyDown(GLFW_KEY_D))
    {
        cameras[currentCameraID].moveRight(5.0);
    }
    if (dag::keyManager->isKeyDown(GLFW_KEY_UP))
    {
        cameras[currentCameraID].pitch(0.1);
    }
    if (dag::keyManager->isKeyDown(GLFW_KEY_DOWN))
    {
        cameras[currentCameraID].pitch(-0.1);
    }
    if (dag::keyManager->isKeyDown(GLFW_KEY_LEFT))
    {
        cameras[currentCameraID].yaw(-0.1);
    }
    if (dag::keyManager->isKeyDown(GLFW_KEY_RIGHT))
    {
        cameras[currentCameraID].yaw(0.1);
    }

    cameraDescriptors[0].data = cameras[currentCameraID].getView();
    cameraDescriptors[1].data = cameras[currentCameraID].getProjection();
    cameraDescriptors[2].data = cameras[currentCameraID].getPosition();
	
    for (size_t i = 0; i < lightDescriptors.size(); i++)
    {
        lightDescriptors[i].data = lights[i].getPosition();
    }
	
    for (auto& entity : entities)
    {
        auto descriptorSet = coordinator->getComponent<DescriptorSet>(entity);

        shaderPrograms[descriptorSet.shaderProgramID].bind();
        RenderingSystem::currentShaderBound = shaderPrograms[descriptorSet.shaderProgramID].id;

        // Updating camera data
        for (auto& descriptor : cameraDescriptors)
        {
            shaderUpdateFuncs[descriptor.fID](descriptor);
        }

        // Updating light data
        for(auto& descriptor : lightDescriptors)
        {
            shaderUpdateFuncs[descriptor.fID](descriptor);
        }

        // Updating model data
        for (auto& descriptor : descriptorSet.descriptors)
        {
            shaderUpdateFuncs[descriptor.fID](descriptor);
        }

        // Draw the mesh
        vaos[descriptorSet.vaoID].draw();
        shaderPrograms[descriptorSet.shaderProgramID].unbind();
    }
}

/**
 * @brief Wrapper for updating a float uniform
 * @param descriptor 
*/
void dag::RenderingSystem::wrapUniform1f(Descriptor const& descriptor)
{
    glUniform1f(glGetUniformLocation(currentShaderBound, descriptor.name.c_str()), std::any_cast<float>(descriptor.data));
}

/**
 * @brief Wrapper for updating an integer uniform
 * @param descriptor 
*/
void dag::RenderingSystem::wrapUniform1i(Descriptor const& descriptor)
{
    glUniform1f(glGetUniformLocation(currentShaderBound, descriptor.name.c_str()), std::any_cast<int32_t>(descriptor.data));
}

/**
 * @brief Wrapper for updating an unsigned int uniform
 * @param descriptor 
*/
void dag::RenderingSystem::wrapUniform1ui(Descriptor const& descriptor)
{
    glUniform1f(glGetUniformLocation(currentShaderBound, descriptor.name.c_str()), std::any_cast<uint32_t>(descriptor.data));
}

/**
 * @brief Wrapper for updating a float2 uniform
 * @param descriptor 
*/
void dag::RenderingSystem::wrapUniform2fv(Descriptor const& descriptor)
{
    glUniform2fv(glGetUniformLocation(currentShaderBound, descriptor.name.c_str()), 1, glm::value_ptr(std::any_cast<glm::vec2>(descriptor.data)));
}

/**
 * @brief Wrapper for updating a float3 uniform
 * @param descriptor 
*/
void dag::RenderingSystem::wrapUniform3fv(Descriptor const& descriptor)
{
    glUniform3fv(glGetUniformLocation(currentShaderBound, descriptor.name.c_str()), 1, glm::value_ptr(std::any_cast<glm::vec3>(descriptor.data)));
}

/**
 * @brief Wrapper for updating a float4 uniform
 * @param descriptor 
*/
void dag::RenderingSystem::wrapUniform4fv(Descriptor const& descriptor)
{
    glUniform4fv(glGetUniformLocation(currentShaderBound, descriptor.name.c_str()), 1, glm::value_ptr(std::any_cast<glm::vec4>(descriptor.data)));
}

/**
 * @brief Wrapper for updating a int2 uniform
 * @param descriptor 
*/
void dag::RenderingSystem::wrapUniform2iv(Descriptor const& descriptor)
{
    glUniform2iv(glGetUniformLocation(currentShaderBound, descriptor.name.c_str()), 1, glm::value_ptr(std::any_cast<glm::ivec2>(descriptor.data)));
}

/**
 * @brief Wrapper for updating a int3 uniform
 * @param descriptor 
*/
void dag::RenderingSystem::wrapUniform3iv(Descriptor const& descriptor)
{
    glUniform3iv(glGetUniformLocation(currentShaderBound, descriptor.name.c_str()), 1, glm::value_ptr(std::any_cast<glm::ivec3>(descriptor.data)));
}

/**
 * @brief Wrapper for updating a int4 uniform
 * @param descriptor 
*/
void dag::RenderingSystem::wrapUniform4iv(Descriptor const& descriptor)
{
    glUniform4iv(glGetUniformLocation(currentShaderBound, descriptor.name.c_str()), 1, glm::value_ptr(std::any_cast<glm::ivec4>(descriptor.data)));
}

/**
 * @brief Wrapper for updating a uint2 uniform
 * @param descriptor 
*/
void dag::RenderingSystem::wrapUniform2uiv(Descriptor const& descriptor)
{
    glUniform2uiv(glGetUniformLocation(currentShaderBound, descriptor.name.c_str()), 1, glm::value_ptr(std::any_cast<glm::uvec2>(descriptor.data)));
}

/**
 * @brief Wrapper for updating a uint3 uniform
 * @param descriptor 
*/
void dag::RenderingSystem::wrapUniform3uiv(Descriptor const& descriptor)
{
    glUniform3uiv(glGetUniformLocation(currentShaderBound, descriptor.name.c_str()), 1, glm::value_ptr(std::any_cast<glm::uvec3>(descriptor.data)));
}

/**
 * @brief Wrapper for updating a uint4 uniform
 * @param descriptor 
*/
void dag::RenderingSystem::wrapUniform4uiv(Descriptor const& descriptor)
{
    glUniform4uiv(glGetUniformLocation(currentShaderBound, descriptor.name.c_str()), 1, glm::value_ptr(std::any_cast<glm::uvec4>(descriptor.data)));
}

/**
 * @brief Wrapper for updating a matrix4x4 uniform
 * @param descriptor 
*/
void dag::RenderingSystem::wrapUniformMatrix4fv(Descriptor const& descriptor)
{
    glUniformMatrix4fv(glGetUniformLocation(currentShaderBound, descriptor.name.c_str()), 1, false, glm::value_ptr(std::any_cast<glm::mat4>(descriptor.data)));
}
