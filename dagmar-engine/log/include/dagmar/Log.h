#pragma once
#include <string>
#include <iostream>

#ifdef _WIN32
#include <windows.h>
#endif

#ifndef NDEBUG

/**
 * @brief Log class
 *
 * Collection of static functions to aid in printing text in the console.
 */
class Log
{
  public:
    // Log Type that can be used
    enum class Type
    {
        Debug,
        Info,
        Warning,
        Error,
        Success
    };

    #ifdef _WIN32
        #define RED "\033[31m"
        #define GREEN "\033[32m"
        #define YELLOW "\033[33m"
        #define MAGENTA "\033[35m"
        #define CYAN "\033[36m"
        #define RESET "\033[0m"
    #else
        #define RED "\e[0;31m"
        #define GREEN "\e[0;32m"
        #define YELLOW "\e[0;33m"
        #define MAGENTA "\e[0;35m"
        #define CYAN "\e[0;36m"
        #define RESET "\033[0m"
    #endif

    /**
         * @brief Print log
         * @param text
         */
    template <typename... Targs>
    static void print(Targs const&... input, Type const& logType)
    {
        switch (logType)
        {
            case Type::Debug:
            {
                debug(input...);
                break;
            }
            case Type::Info:
            {
                info(input...);
                break;
            }
            case Type::Warning:
            {
                warning(input...);
                break;
            }
            case Type::Error:
            {
                error(input...);
                break;
            }
            case Type::Success:
            {
                success(input...);
                break;
            }
            default:
            {
                break;
            }
        };
    }

    static void output()
    {
        std::cout << RESET << std::endl;
    }

    template <typename T, typename... Targs>
    static void output(T head, Targs... tail)
    {
        std::cout << head;
        output(tail...);
    }

    /**
         * @brief Debug log
         * @param text
         */
    template <typename... Targs>
    static void debug(Targs const&... input)
    {
        std::cout << MAGENTA << "[DEBUG]: ";
        output(input...);
    }
    /**
         * @brief Info log
         * @param text
         */
    template <typename... Targs>
    static void info(Targs const&... input)
    {
        std::cout << CYAN << "[INFO]: ";
        output(input...);
    }

    /**
         * @brief Warning log
         * @param text
         */
    template <typename... Targs>
    static void warning(Targs const&... input)
    {
        std::cout << YELLOW << "[WARNING]: ";
        output(input...);
    }

    /**
         * @brief Error log
         * @param text
         */
    template <typename... Targs>
    static void error(Targs const&... input)
    {
        std::cout << RED << "[ERROR]: ";
        output(input...);
    }

    /**
         * @brief Success log
         * @param text
         */
    template <typename... Targs>
    static void success(Targs const&... input)
    {
        std::cout << GREEN << "[SUCCESS]: ";
        output(input...);
    }
};

#else

class Log
{
  public:
    // Log Type that can be used
    enum class Type
    {
        Debug,
        Info,
        Warning,
        Error,
        Success
    };

    template <typename... Targs>
    static void print(Targs const&... input, Type const& logType)
    {
    }
    static void output()
    {
    }
    template <typename T, typename... Targs>
    static void output(T head, Targs... tail)
    {
    }
    template <typename... Targs>
    static void debug(Targs const&... input)
    {
    }
    template <typename... Targs>
    static void info(Targs const&... input)
    {
    }
    template <typename... Targs>
    static void warning(Targs const&... input)
    {
    }
    template <typename... Targs>
    static void error(Targs const&... input)
    {
    }
    template <typename... Targs>
    static void success(Targs const&... input)
    {
    }
};

#endif
