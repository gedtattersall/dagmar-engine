#include "dagmar/Scene.h"

dag::Scene::Scene(){
    currentEntity = -1;
}

void dag::Scene::createEntity(std::string name){
    
    entities.push_back(coordinator->createEntity());

    coordinator->addComponent(entities[entities.size() - 1], dag::Name{name});
    
    currentEntity = entities.size() - 1;
    
    recomputeSceneGraph();
}

void dag::Scene::destroyEntity(dag::Entity const& entity){
    coordinator->destroyEntity(entity);

    for(size_t i = 0; i < entities.size() - 1; ++i){
        if(entity == entities[i]){
            for(size_t j = i; j < entities.size() - 1; ++j){
                entities[j] = entities[j+1];
            }
        }
        break;
    }
    entities.erase(entities.end() - 1);

    recomputeSceneGraph();
}

void dag::Scene::startup(){

}

void dag::Scene::shutdown(){

}

std::string dag::Scene::getName() const {
    return "Scene";
}


void dag::Scene::recomputeSceneGraph(){
    // clear before
    sceneGraph.resize(entities.size());
    int i = 0;
    for(auto const& entity : entities){
        auto nameComponent = coordinator->getComponent<dag::Name>(entity);
        sceneGraph[i] = new char[nameComponent.name.size() + 1];
        std::strcpy (sceneGraph[i++], nameComponent.name.c_str());
    }
}