#pragma once

#include "dagmar/Components.h"
#include "dagmar/Coordinator.h"
#include <vector>
#include <cstring>

namespace dag
{
    class Scene : public Module
    {
      public:
        std::shared_ptr<dag::Coordinator> coordinator;
        std::vector<dag::Entity> entities;
        // to be used in IMGUI as sceneGraph.data() for pure pointer
        std::vector<char *> sceneGraph;
        int currentEntity;

      public:
        Scene();
        void createEntity(std::string name);
        void destroyEntity(dag::Entity const& entity);
        template <typename T>
        void addComponent(dag::Entity const& entity, T const& component){
            coordinator->addComponent<T>(entity, component);
        }
        template <typename T>
        void removeComponent(dag::Entity const& entity){
            coordinator->removeComponent<T>(entity);
        }

        void recomputeSceneGraph();
        std::string getName() const override;

        // Starts up the module
        void startup() override;

        // Shuts down the module
        void shutdown() override;
    };

    inline std::shared_ptr<Scene> scene(new Scene());
} // namespace dag