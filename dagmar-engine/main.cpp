// Include standard headers
#include <cstdio>
#include <cstdlib>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include <string>
#include <vector>
#include <random>

using namespace glm;

#define TINYOBJLOADER_IMPLEMENTATION
#include "dagmar/Components.h"
#include "dagmar/Coordinator.h"
#include "dagmar/FileSystem.h"
#include "dagmar/KeyManager.h"
#include "dagmar/Log.h"
#include "dagmar/Mesh.h"
#include "dagmar/ModuleLifecycle.h"
#include "dagmar/PhysicsSystem.h"
#include "dagmar/Renderer.h"
#include "dagmar/RenderingSystem.h"
#include "dagmar/ResourceManager.h"
#include "dagmar/VertexArrayObject.h"
#include "dagmar/WindowManager.h"
#include "dagmar/TimeManager.h"
#include "dagmar/Vertex.h"
#include "dagmar/Scene.h"


int main(void)
{
    dag::ModuleLifecycle::get().startup();

    auto& ecsCoord = dag::coordinator;
    auto renderingSystem = ecsCoord->getSystem<dag::RenderingSystem>();
    auto physicsSystem = ecsCoord->registerSystem<dag::PhysicsSystem>();
    physicsSystem->coordinator = ecsCoord;
    physicsSystem->startup();

    {
        std::vector<dag::Vertex> tempVertices = {
            dag::Vertex{.position = glm::vec3(-0.1f, -0.1f, 0.0f), .normal = glm::vec3(0.0, 0.0f, 1.0f)},
            dag::Vertex{.position = glm::vec3(0.1f, -0.1f, 0.0f), .normal = glm::vec3(0.0, 0.0f, 1.0f)},
            dag::Vertex{.position = glm::vec3(0.1f, 0.1f, 0.0f), .normal = glm::vec3(0.0, 0.0f, 1.0f)},
            dag::Vertex{.position = glm::vec3(-0.1f, 0.1f, 0.0f), .normal = glm::vec3(0.0, 0.0f, 1.0f)}
        };

        std::vector<float> vertices;
        for(auto& tv : tempVertices)
        {
            std::vector<float> tvFloat = tv.toFloatVector();
            vertices.insert(vertices.end(), tvFloat.begin(), tvFloat.end());
        }

        std::vector<uint32_t> indices = {0, 1, 2, 2, 3, 0};

        dag::Mesh mesh = dag::Mesh(vertices, indices);
       // rsc.getMesh(0, mesh);

        std::vector<dag::VertexAttribute> atts = {
            dag::VertexAttribute(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0),
            dag::VertexAttribute(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)))
            };

        renderingSystem->vaos.push_back(mesh.generateVAO(atts));
    }

    dag::scene->coordinator = ecsCoord;
    ecsCoord->registerComponent<dag::Name>();

    dag::scene->createEntity("First entity");
    
    dag::scene->createEntity("Second entity");
    dag::scene->createEntity("Third entity");
    


    int i = 0;
    for (auto& entity : dag::scene->entities)
    {
        

        if (i == 0)
        {
            ecsCoord->addComponent(entity, dag::Transform{glm::vec3(-1.0f, 0.0f, 0.0f)});
            ecsCoord->addComponent(entity, dag::RigidBody{.impulse = glm::vec3(20.0f, 0.0f, 0.0f), .mass = 0.05f});
            ecsCoord->addComponent(entity, dag::Collision{.radius = 0.1, .center = glm::vec3(0.0f)});
        }
        else 
        {
            ecsCoord->addComponent(entity, dag::Transform{glm::vec3(1.0f, 0.0f, 0.0f)});
            ecsCoord->addComponent(entity, dag::RigidBody{.impulse = glm::vec3(-20.0f, 0.0f, 0.0f), .mass = 0.049f});
            ecsCoord->addComponent(entity, dag::Collision{.radius = 0.1, .center = glm::vec3(0.0f)});
        }
        ++i;

        ecsCoord->getComponent<dag::Transform>(entity).updateModelMatrix();

        dag::DescriptorSet descriptorSet;

    	// IDs in the vector rather than their GL ID
        descriptorSet.shaderProgramID = 0;
        descriptorSet.vaoID = 0;

        dag::Descriptor descriptorModel;
        descriptorModel.fID = dag::RenderingSystem::UniformFuncs::UniformMatrix4fv;
        descriptorModel.name = "model";
        descriptorModel.data = ecsCoord->getComponent<dag::Transform>(entity).modelMatrix;

        descriptorSet.descriptors.push_back(descriptorModel);
        ecsCoord->addComponent(entity, descriptorSet);
    }


    // Check if the ESC key was pressed or the window was closed
    while (glfwGetKey(dag::windowManager->window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
           !glfwWindowShouldClose(dag::windowManager->window))
    {
        dag::timeManager->updateFrameStart();

        glfwPollEvents();

        for (auto& entity : dag::scene->entities)
        {
            auto& descriptorSet = ecsCoord->getComponent<dag::DescriptorSet>(entity);
            auto& transform = ecsCoord->getComponent<dag::Transform>(entity);
            transform.updateModelMatrix();
            descriptorSet.descriptors[0].data = ecsCoord->getComponent<dag::Transform>(entity).modelMatrix;
        }

        dag::renderer->draw();
    }

    dag::ModuleLifecycle::get().shutdown();

    return 0;
}
