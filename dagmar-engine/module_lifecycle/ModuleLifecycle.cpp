#include "dagmar/ModuleLifecycle.h"

/**
* @brief This class is a singleton, this is how it should be retrieved.
*/
dag::ModuleLifecycle& dag::ModuleLifecycle::get()
{
	Log::debug("Called ModuleLifecycle::get()");
	static ModuleLifecycle singleton;
	return singleton;
}

/**
* @brief Function to start up all modules in the desired order
*/
void dag::ModuleLifecycle::startup()
{
	Log::debug("Called ModuleLifecycle()");
	for (int m = 0; m < modules.size(); ++m)
	{
		Log::debug(std::string("Calling Startup for ") + modules[m]->getName());
		modules[m]->startup();
	}
}

/**
* @brief Function to shut down all modules in the desired order 
* (roughly the reverse of the start up)
*/
void dag::ModuleLifecycle::shutdown()
{
    shuttingDown = true;
	Log::debug("Called ~ModuleLifecycle()");
	for (int m = modules.size() - 1; m > -1; --m)
	{
		Log::debug(std::string("Calling Shutdown for ") + modules[m]->getName());
		modules[m]->shutdown();
	}
}