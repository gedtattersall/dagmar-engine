#version 450
#extension GL_ARB_separate_shader_objects : enable

/////////////// STRUCTS ///////////////////////
////////////////////////////////////////////////

/////////////////// VS INPUT ///////////////////
layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
// layout(location = 2) in vec4 inColor;
// layout(location = 3) in vec2 inUV;
////////////////////////////////////////////////

///////////////// VS OUTPUTS /////////////////// 
layout(location = 0) out vec3 outFragPos;
layout(location = 1) out vec3 outFragNormal;
layout(location = 2) out vec3 outLightPos;
layout(location = 3) out vec3 outCameraPos;
// layout(location = 4) out vec4 outFragColor;
// layout(location = 5) out vec2 outUV;
////////////////////////////////////////////////

////////////////// UNIFORMS ////////////////////
layout(location = 0) uniform mat4 projection;
layout(location = 1) uniform mat4 view;
layout(location = 2) uniform mat4 model;
layout(location = 3) uniform vec3 cameraPosition;
layout(location = 4) uniform vec3 lightPosition;
////////////////////////////////////////////////

void main()
{
    gl_Position = projection * view * model * vec4(inPosition, 1.0f);

    outFragPos = vec3(model * vec4(inPosition, 1.0f));
    // outFragColor = inColor;
    // outUV = inUV;
    outFragNormal = normalize((transpose(inverse(model)) * vec4(inNormal, 1.0)).xyz);
    outLightPos = normalize(lightPosition - outFragPos);
    outCameraPos = normalize(cameraPosition - outFragPos);
}