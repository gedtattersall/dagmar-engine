#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>
#include <string>
#include <iostream>
#include "dagmar/ModuleLifecycle.h"
#include "dagmar/ResourceManager.h"
#include "dagmar/FileSystem.h"


TEST_CASE("Resources Tests")
{
    //dag::ModuleLifecycle::get().startup();

    SECTION("TEST 1")
    {
        dag::ResourceManager test1;
        test1.parseOBJ("../test_assets/duck.obj");
        std::cout << "Read Files" << std::endl;
        test1.writeBin("../test_assets/duck.obj");
        std::cout << "Read Files" << std::endl;
        test1.readBin("../test_assets/duck.dat");
       
        // Model->Shapes->Faces->VertexIndices
        for (int i = 0; i < test1.models.size(); i++)
        {
            //Vertex Position check
            for (int j = 0; j < test1.models[i].vertices.vertices.size(); j++)
            {
                REQUIRE(test1.models[i].vertices.vertices[j] == test1.outModels[i].vertices.vertices[j]);
            }
            //Vertex Normal check
            for (int j = 0; j < test1.models[i].vertices.normals.size(); j++)
            {
                REQUIRE(test1.models[i].vertices.normals[j] == test1.outModels[i].vertices.normals[j]);
            }
            //Vertex TextureCoord check
            for (int j = 0; j < test1.models[i].vertices.textcoords.size(); j++)
            {
                REQUIRE(test1.models[i].vertices.textcoords[j] == test1.outModels[i].vertices.textcoords[j]);
            }
            //Vertex Colour check
            for (int j = 0; j < test1.models[i].vertices.colors.size(); j++)
            {
                REQUIRE(test1.models[i].vertices.colors[j] == test1.outModels[i].vertices.colors[j]);
            }

            std::cout << "Vertices Done" << std::endl;

            //Shapes
            for (int j = 0; j < test1.models[i].shapes.size(); j++)
            {
                //ShapeName
                REQUIRE(test1.models[i].shapes[j].shapeName == test1.outModels[i].shapes[j].shapeName);

                for (int k = 0; k < test1.models[i].shapes[j].faces.size(); k++)
                {
                    //Comparing VertexIndicies
                    for (int l = 0; l < test1.models[i].shapes[j].faces[k].vertexIndex.size(); l++)
                    {
                        REQUIRE(test1.models[i].shapes[j].faces[k].vertexIndex[l] == test1.outModels[i].shapes[j].faces[k].vertexIndex[l]);
                    }
                    //Comparing NormalIndicies
                    for (int l = 0; l < test1.models[i].shapes[j].faces[k].normalIndex.size(); l++)
                    {
                        REQUIRE(test1.models[i].shapes[j].faces[k].normalIndex[l] == test1.outModels[i].shapes[j].faces[k].normalIndex[l]);
                    }
                    //Comparing TextureCoordsIndicies
                    for (int l = 0; l < test1.models[i].shapes[j].faces[k].texcoordIndex.size(); l++)
                    {
                        REQUIRE(test1.models[i].shapes[j].faces[k].texcoordIndex[l] == test1.outModels[i].shapes[j].faces[k].texcoordIndex[l]);
                    }
                }
            }

            for (int j = 0; j < test1.models[i].materials.size(); j++)
            {
                REQUIRE(test1.models[i].materials[j].name == test1.outModels[i].materials[j].name);
                REQUIRE(test1.models[i].materials[j].ambience == test1.outModels[i].materials[j].ambience);
                REQUIRE(test1.models[i].materials[j].diffuse == test1.outModels[i].materials[j].diffuse);
                REQUIRE(test1.models[i].materials[j].specular == test1.outModels[i].materials[j].specular);
                REQUIRE(test1.models[i].materials[j].emission == test1.outModels[i].materials[j].emission);
                REQUIRE(test1.models[i].materials[j].dissolve == test1.outModels[i].materials[j].dissolve);
                REQUIRE(test1.models[i].materials[j].textureAmbience == test1.outModels[i].materials[j].textureAmbience);
                REQUIRE(test1.models[i].materials[j].textureDiffuse == test1.outModels[i].materials[j].textureDiffuse);
                REQUIRE(test1.models[i].materials[j].textureSpecular == test1.outModels[i].materials[j].textureSpecular);
                REQUIRE(test1.models[i].materials[j].textureAlpha == test1.outModels[i].materials[j].textureAlpha);
            }
        }
    }

    //dag::ModuleLifecycle::get().shutdown();
}
