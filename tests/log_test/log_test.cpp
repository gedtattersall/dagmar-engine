#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>
#include <string>
#include <iostream>
#include <sstream>
#include <functional>
#include <dagmar/Log.h>

#ifndef NDEBUG

std::string retrieve_string_of_func(const std::string& text, const Log::Type& logType,
        std::function<void (const std::string&, const Log::Type&)> func) {
    std::ostringstream oss;
    // Acquire the cout buffer
    std::streambuf* coutStreambuffer = std::cout.rdbuf();
    std::cout.rdbuf(oss.rdbuf());

    func(text, logType);

    // Restore the cout buffer
    std::cout.rdbuf(coutStreambuffer);
    return oss.str();
}

std::string print_test(const std::string& text, const Log::Type& logType) {
    return retrieve_string_of_func(text, logType, Log::print<std::string>);
}

TEST_CASE("Testing Logger::debug", "[print_test]") {
    std::string against = MAGENTA "[DEBUG]: test_debug" RESET "\n";
    REQUIRE(print_test("test_debug", Log::Type::Debug) == against);
}

TEST_CASE("Testing Logger::info", "[print_test]") {
    std::string against = CYAN "[INFO]: test_info" RESET "\n";
    REQUIRE(print_test("test_info", Log::Type::Info) == against);
}

TEST_CASE("Testing Logger::warning", "[print_test]") {
    std::string against = YELLOW "[WARNING]: test_warning" RESET "\n";
    REQUIRE(print_test("test_warning", Log::Type::Warning) == against);
}

TEST_CASE("Testing Logger::error", "[print_test]") {
    std::string against = RED "[ERROR]: test_error" RESET "\n";
    REQUIRE(print_test("test_error", Log::Type::Error) == against);
}

TEST_CASE("Testing Logger::success", "[print_test]") {
    std::string against = GREEN "[SUCCESS]: test_success" RESET "\n";
    REQUIRE(print_test("test_success", Log::Type::Success) == against);
}

#else

TEST_CASE("Testing Logger::Release")
{
    REQUIRE(1 == 1);
}

#endif
