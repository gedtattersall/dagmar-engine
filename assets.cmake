set(ASSETS_DIR ${CMAKE_CURRENT_SOURCE_DIR}/assets)
set(ASSETS_INSTALL_DIR ${CMAKE_INSTALL_PREFIX}/assets)
set(ASSETS_BUILD_DIR ${PROJECT_BINARY_DIR}/assets)

string(FIND ${CMAKE_GENERATOR} "Visual Studio" VS_GENERATOR)

if(NOT (${VS_GENERATOR} STREQUAL "-1"))
    set(ASSETS_FROM_MAIN "../../assets/")
else()
    set(ASSETS_FROM_MAIN "../assets/")
endif()

# get all files in asset folder
file(GLOB_RECURSE ASSET_FILES
	${ASSETS_DIR}/*
)

# this will be used to ensure unique naming
set(ASSET_COUNT 0)

# for each file we found
foreach(ASSET_FILE IN LISTS ASSET_FILES)
	# get the path relative to the asset folder
	file(RELATIVE_PATH RELATIVE_TO_ASSET_FOLDER ${ASSETS_DIR} ${ASSET_FILE})

	# the command to actually copy out of date assets
	add_custom_command(OUTPUT ${ASSETS_INSTALL_DIR}/${RELATIVE_TO_ASSET_FOLDER} ${ASSETS_BUILD_DIR}/${RELATIVE_TO_ASSET_FOLDER}
		COMMENT "Copying asset: ${RELATIVE_TO_ASSET_FOLDER}"
		COMMAND ${CMAKE_COMMAND} -E copy ${ASSET_FILE} ${ASSETS_INSTALL_DIR}/${RELATIVE_TO_ASSET_FOLDER}
		COMMAND ${CMAKE_COMMAND} -E copy ${ASSET_FILE} ${ASSETS_BUILD_DIR}/${RELATIVE_TO_ASSET_FOLDER}
		DEPENDS ${ASSET_FILE}
	)

	# this tracks whether or not something is out of date
	add_custom_target(asset_${ASSET_COUNT} ALL
	   DEPENDS ${ASSETS_INSTALL_DIR}/${RELATIVE_TO_ASSET_FOLDER}
	   DEPENDS ${ASSETS_BUILD_DIR}/${RELATIVE_TO_ASSET_FOLDER}
	)

	MATH(EXPR ASSET_COUNT "${ASSET_COUNT}+1")
endforeach()
