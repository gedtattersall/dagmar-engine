#!/bin/bash

mkdir build
pushd build
cmake .. -DCMAKE_BUILD_TYPE=Debug
cmake --build .
popd
ln -s ./build/compile_commands.json . 
